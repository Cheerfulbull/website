$(function() {
    $('.toggle > a').on('click', function(e) {
        e.preventDefault();

        $(this).addClass('selected').siblings().removeClass('selected');
    });

    $('.input--repeater a.add-row').on('click', function(e) {
        var $row = $(this).siblings('.row:eq(0)').clone();

        var group = $(this).closest('.input--repeater').data('group');
        var index = $(this).siblings('.row').length;
        $row.find('input').each(function() {
            var name = group + '[' + index + '][' + $(this).data('name') + ']';


            
            $(this).val('').attr('name', name).attr('id', name);
            $(this).siblings('label').attr('for', name);
        });

        $row.insertBefore($(this));

        e.preventDefault();
    });

    // Remove image
    $('.form-image-preview .form-image-preview-remove').on('click', function() {
        $(this).closest('form').append($('<input>', { type: 'hidden', name: 'removeImage' }));
        $(this).closest('.form-image-preview').remove();
    });

    function uploadImage(file, callback, errorCallback) {
        // Upload image
        var ajaxData = new FormData();

        ajaxData.append('upload', file);
        ajaxData.append('token', encodeURIComponent(document.dtw.csrf));

        $.ajax({
            type: 'POST',
            url: document.dtw.api + 'images/upload',
            data: ajaxData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {
                if (typeof(callback) == 'function') {
                    callback(data.data.url);
                }
            },
            error: function(data) {
                if (typeof(errorCallback) == 'function') {
                    errorCallback(data);
                }
            }
        });
    }

    function setupMarkdownEditors() {
        // Load JS
        $.getScript(document.dtw.static + "/vendor/simplemde/simplemde.min.js", function() {
            $(".markdown-editor textarea").each(function() {
                var $target = $(this);
                var editor = new SimpleMDE({
                    autoDownloadFontAwesome: false,
                    element: $target[0],
                    toolbar: [
                        "bold", "italic", "heading-2", "heading-3", "|",
                        "code", "quote", {
                            name: "spoiler",
                            action: function customFunction(editor) {
                                _replaceSelection(editor.codemirror, false, ["<!- ", " -!>"]);
                            },
                            className: "fa fa-eye-slash",
                            title: "Spoiler",
                        }, "|",
                        "link", {
                            name: "mention",
                            action: function customFunction(editor) {
                                _replaceSelection(editor.codemirror, false, ["@", ""]);
                            },
                            className: "fa fa-at",
                            title: "Mention user",
                        }, {
                            name: "level",
                            action: function customFunction(editor) {
                                _replaceSelection(editor.codemirror, false, ["?", ""]);
                            },
                            className: "fa fa-flask",
                            title: "Mention level",
                        }, "table", "unordered-list", "|",
                        "preview"
                    ],
                    spellChecker: false,
                    tabSize: 4,
                    forceSync: true,
                    promptURLs: true,
                    status: ["lines", "words", "cursor", {
                        className: "help",
                        defaultValue: function(el) {
                            el.innerHTML = "<a target='_blank' href='/help/discussions/markdown'>Markdown enabled</a>";
                        }
                    }],
                    previewRender: function(plainText, preview) {
                        $.ajax(document.dtw.api + 'markdown/parse', {
                            data: { 'markup': plainText },
                            type: 'POST'
                        }).done(function(data) {
                            preview.innerHTML = data.data ? data.data : '';
                        });

                        return "Loading...";
                    }
                });

                // Submit on ctrl+enter
                var keys = editor.codemirror.getOption("extraKeys");
                keys["Ctrl-Enter"] = function() {
                    $target.closest('form').submit();
                };
                editor.codemirror.setOption("extraKeys", keys);

                // Fire change event for draft
                editor.codemirror.on("change", function(){
                    $target.trigger('change');
                });

                // Handle drag and paste image
                var $editor = editor.codemirror.getWrapperElement();

                $($editor).on('dragover dragenter', function() {
                    $(this).addClass('is-dragover');
                }).on('dragleave dragend drop', function() {
                    $(this).removeClass('is-dragover');
                }).on('drop', function(e) {
                    e = e.originalEvent;

                    var items = e.dataTransfer.items || e.dataTransfer.files || [];

                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];

                        var file = item.getAsFile();
                        if (file && ['image/jpeg', 'image/png', 'image/jpg', 'image/gif'].indexOf(file.type) >= 0) {
                            uploadImage(file, function(url) {
                                editor.codemirror.replaceSelection('![]('+url+')');
                            });
                        }
                    }

                    e.preventDefault();
                }).on('paste', function(e) {
                    e = e.originalEvent;

                    if (typeof e.clipboardData === "object") {
                        var result = false,
                            items = e.clipboardData.items || e.clipboardData.files || [];

                        for (var i = 0; i < items.length; i++) {
                            var item = items[i];

                            var file = item.getAsFile();
                            if (file && ['image/jpeg', 'image/png', 'image/jpg', 'image/gif'].indexOf(file.type) >= 0) {
                                uploadImage(file, function(url) {
                                    editor.codemirror.replaceSelection('![]('+url+')');
                                });
                            }
                        }
                    }

                    if (result) { e.preventDefault(); }
                }).on('keyup', function(e) {
                    var cm = editor.codemirror,
                        doc = cm.getDoc(),
                        cursor = doc.getCursor(),
                        line = doc.getLine(cursor.line),
                        position = cursor.ch,
                        regex = /(^|\s)([@|\?])(\S*)$/;

                    // Trim line to character position
                    line = line.substr(0, position);

                    var matches = regex.exec(line);

                    if (matches) {
                        var match = matches[3];

                        if (!match.length) {
                            return;
                        }

                        var subject = 'users';
                        if (matches[2] == '?') {
                            subject = 'playground';
                        }

                        // start auto complete
                        var start = {line: cursor.line, ch: cursor.ch - match.length},
                            end = {line: cursor.line, ch: cursor.ch};

                        var position = cm.cursorCoords(start);
                        position.top = position.bottom + 10;

                        autocomplete.search(
                            subject,
                            match,
                            {
                                top: position.top,
                                left: position.left
                            },
                            function(result) {
                                doc.replaceRange(result + ' ', start, end);
                                doc.setCursor({line: cursor.line, ch: cursor.ch - match.length + result.length + 2});
                                cm.focus();
                            }
                        );
                    } else {
                        autocomplete.remove();
                    }
                });
            });
        });

        window.setupMarkdownEditors = setupMarkdownEditors;

        // Load CSS
        $('<link>')
        .appendTo('head')
        .attr({
            type: 'text/css', 
            rel: 'stylesheet',
            href: document.dtw.static + "/vendor/simplemde/simplemde.min.css"
        });
    }

    function _replaceSelection(cm, active, startEnd, url) {
        if(/editor-preview-active/.test(cm.getWrapperElement().lastChild.className))
            return;

        var text;
        var start = startEnd[0];
        var end = startEnd[1];
        var startPoint = cm.getCursor("start");
        var endPoint = cm.getCursor("end");
        if(url) {
            end = end.replace("#url#", url);
        }
        if(active) {
            text = cm.getLine(startPoint.line);
            start = text.slice(0, startPoint.ch);
            end = text.slice(startPoint.ch);
            cm.replaceRange(start + end, {
                line: startPoint.line,
                ch: 0
            });
        } else {
            text = cm.getSelection();
            cm.replaceSelection(start + text + end);

            startPoint.ch += start.length;
            if(startPoint !== endPoint) {
                endPoint.ch += start.length;
            }
        }
        cm.setSelection(startPoint, endPoint);
        cm.focus();
    }


    // Markdown editor
    if ($(".markdown-editor textarea").length) {
        setupMarkdownEditors();
    }

    // Drafts
    $('form[data-draft]').each(function() {
        var $form = $(this),
            key = 'draft:' + $form.data('draft');

        var draft = localStorage.getItem(key);

        if (draft) {
            draft = JSON.parse(draft);

            if (Object.keys(draft).length) {
                $.each(draft, function(key, value) {
                    $form.find(':input[name=' + key + ']').val(value);
                });

                // Add clear draft link
                $('<a>', { 'text': 'Clear draft', 'href': '#' }).on('click', function(e) {
                    e.preventDefault();

                    $form[0].reset();

                    localStorage.removeItem(key);

                    $(this).remove();
                }).appendTo($form);
            }
        }

        // Handle changes 
        $form.find(':input').on('change', function() {
            var $fields = $form.find(':input:visible, textarea'),
                draft = {};

            $fields.each(function() {
                if ($(this).attr('name')) {
                    draft[$(this).attr('name')] = $(this).val();
                }
            });

            localStorage.setItem(key, JSON.stringify(draft));
        });
    });

    $('form[data-draft]').on('submit', function() {
        var $form = $(this),
            key = 'draft:' + $form.data('draft');

        localStorage.removeItem(key);
    });
});