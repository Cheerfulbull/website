var timer_start = new Date().getTime();
(function timer() {
    $('.level-timer').each(function() {
        var now = new Date().getTime();
        var time = now - timer_start;
        time = $(this).attr('data-time') - time/1000;
        if (time <= 0)
            time = 0;

        if (time < 1) {
            $(this).addClass('c-error')
        }

        $(this).html('Time remaining: <span>'+time.toFixed(2)+' seconds</span>');
    });

    setTimeout(timer, 100);
})();