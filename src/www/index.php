<?php
    error_reporting(E_ERROR);
    ini_set('display_errors', 0);

    date_default_timezone_set('UTC');

    require __DIR__ . '/../vendor/autoload.php';

    // Can we serve them static HTML?
    \dtw\utils\StaticCache::load();

    $klein = new \Klein\Klein();

    $klein->respond(function($request, $response, $service, $app) use ($klein) {
        $app->DtW = \dtw\DtW::getInstance();

        $app->DtW->tmpl->twig->addGlobal('uri', $request->pathname());

        // TODO
        if (isset($_GET['matrix'])) {
            $app->DtW->tmpl->twig->addGlobal('matrix', $app->DtW->user->getMatrixToken());
        }

        // Mark notificatin read
        if (isset($_GET['notification'])) {
            $app->DtW->user->notifications->markAsRead($_GET['notification']);
        }

        // Load flashes
        \dtw\utils\Flash::load();

        // Maintenance mode
        if (isset($_GET['maintenance'])) {
            $_SESSION['maintenance'] = $_GET['maintenance'];
        }

        if (\dtw\DtW::$config->get('maintenance') && $_SESSION['maintenance'] !== \dtw\DtW::$config->get('maintenance')) {
            echo $app->DtW->tmpl->render('maintenance.twig', array());

            $response->send();
            $klein->skipRemaining();
        }
    });

    // CSRF check
    $klein->respond('POST', '*', function($request, $response, $service, $app) use ($klein) {
        if (count($_POST) == 0) {
            return;
        }

        // Dont check for Playground levels
        if (substr($string, 0, strlen('/playground/')) === '/playground/') {
            return;
        }

        if ($app->DtW->user->isAuth() && (!isset($_POST['token']) || !\dtw\utils\CSRF::check($_POST['token']))) {
            \dtw\utils\Flash::add('Invalid CSRF token', 'error');
            $response->code(403);
            $service->back();
            $response->send();
            $klein->skipRemaining();
            die();
        }
    });

    $routes = scandir(__DIR__ . "/../routes");
    $routes = array_diff($routes, array('.', '..', 'home.php'));

    $klein->with("/", __DIR__ . "/../routes/home.php");
    foreach($routes as $controller) {
        $controller = basename($controller, '.php');
        $klein->with("/$controller", __DIR__ . "/../routes/$controller.php");
    }

    $klein->respond(function ($request, $response, $service, $app) use ($klein) {
        \dtw\utils\StaticCache::save($response->body());
    });

    // Handle error pages
    $klein->onHttpError(function ($code, $router) {
        if ($code == '404' || $code == '403' || $code == '405' || $code == '500') {
            $DtW = \dtw\DtW::getInstance();

            switch ($code) {
                case '403': $code .= ' - Forbidden'; $message = null; break;
                case '404': $code .= ' - Page not found'; $message = 'The page you are looking for doesn\'t exist.'; break;
                case '405': $code .= ' - Method not allowed'; $message = null; break;
                case '500': $code .= ' - Internal server error'; $message = 'Something went wrong on our end.'; break;
            }

            // Get flash
            $flash = \dtw\utils\Flash::get();
            if ($flash[0] && $flash[0]->type && $flash[0]->type == 'error') {
                $message = $flash[0]->msg;
            }

            echo $DtW->tmpl->render('error.twig', array('code' => $code, 'message' => $message));
        }
    });

    $klein->dispatch();
