<?php
    $this->respond('GET', '', function ($request, $response, $service, $app) {
        $breadcrumb = array(
            'Help' => '/help'
        );

        $index = \dtw\Help::getIndex();

        return $app->DtW->tmpl->render('includes/block-index.twig', array('breadcrumb' => $breadcrumb, 'index' => $index));
    });

    $this->respond('GET', '/contact', function ($request, $response, $service, $app) {
        $breadcrumb = array(
            'Help' => '/help',
            'Contact' => '/help/contact'
        );

        $form = new \dtw\utils\Form("Contact Us", "Send", array('block' => true));

        $args = array();
        if (isset($_GET['message'])) {
            $args['value'] = $_GET['message'];
        }
        $form->addField('Message', "markdown", $args);


        if (!$app->DtW->user->isAuth()) {
            $form->addField('Email', "email");
        }

        $tickets = \dtw\tickets\Tickets::getMyTickets();

        echo $app->DtW->tmpl->render('help/tickets.twig', array('breadcrumb' => $breadcrumb, 'form' => $form, 'tickets' => $tickets));
        die();
    });

    $this->respond('GET', '/contact/[i:ticketID]', function ($request, $response, $service, $app) {
        $breadcrumb = array(
            'Help' => '/help',
            'Contact' => '/help/contact'
        );

        if ($app->DtW->user->hasPrivilege('tickets')) {
            $breadcrumb = array(
                'Moderation' => '/moderation',
                'Tickets' => '/moderation/tickets'
            );
        }

        try {
            $ticket = \dtw\tickets\Tickets::get($request->ticketID);

            if (isset($_GET['status'])) {
                $ticket->setStatus($_GET['status']);
                $response->redirect($ticket->permalink);

                $response->send();
                $this->skipRemaining();
            }

            $ticket->markAsRead();

            $form = new \dtw\utils\Form("Reply", "Send", array('block' => true, 'action' => ''));
            $form->addField('Message', "markdown");

            echo $app->DtW->tmpl->render('help/ticket.twig', array('breadcrumb' => $breadcrumb, 'form' => $form, 'ticket' => $ticket));
            die();
        } catch (\Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/help/contact');
        }
    });

    $this->respond('POST', '/contact/[i:ticketID]?', function ($request, $response, $service, $app) {
        try {
            if ($request->ticketID) {
                $ticket = \dtw\tickets\Tickets::get($request->ticketID);
                $ticket->addMessage($_POST['message']);
                \dtw\utils\Flash::add('Reply added', 'success');
                $service->back();
            } else {
                $ticket = \dtw\tickets\Tickets::new($_POST);
                \dtw\utils\Flash::add('Ticket created', 'success');

                $response->redirect($ticket);
            }
        } catch (\Exception $e) {
            \dtw\utils\Form::storeResponses();
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $service->back();
        }
    });

    $this->respond('GET', '/medals', function ($request, $response, $service, $app) {
        $breadcrumb = array(
            'Help' => '/help',
            'Medals' => '/help/medals'
        );

        $medalGroups = \dtw\Medals::getMedalsGrouped();

        echo $app->DtW->tmpl->render('help/medals.twig', array('breadcrumb' => $breadcrumb, 'medalGroups' => $medalGroups));
        die();
    });

    $this->respond('GET', '/[s:topic]/[:page]', function ($request, $response, $service, $app) {
        $args = array();

        $app->DtW->load('Help');

        try {
            $args['page'] = \dtw\Help::getContent($request->topic, $request->page);

            $args['breadcrumb'] = array(
                'Help' => '/help',
                $args['page']->topic => '/help',
                $args['page']->title => '/help/' . $request->topic . '/' . $request->page
            );

            return $app->DtW->tmpl->render('help/page.twig', $args);
        } catch (\Exception $e) {
            echo $e->getMessage();
            $this->abort(404);
        }
    });