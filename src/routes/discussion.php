<?php
    $this->respond(function($request, $response, $service, $app) {
        $app->DtW->load('Discussions');
    });

    /*
     * Full list of topics
     */
    $this->respond('GET', 's/[:topic]?/[popular|latest|no-replies|top:order]?/[i:page]?', function ($request, $response, $service, $app) {
        $app->DtW->load('Discussions');
        $page = $request->page ? $request->page : 1;
        $order = $request->order ? $request->order : 'popular';

        $topics = $app->DtW->discussions->getTopics();

        // Does topic exist?
        if ($request->topic && $request->topic !== 'all') {
            try {
                $topic = $app->DtW->discussions->getTopic($request->topic);

                if ($topic->parent) {
                    $topic->parent = $app->DtW->discussions->getTopic($topic->parent);
                }

                if (!$topic) {
                    $topic = 'all';
                }
            } catch (Exception $e) {
                \dtw\utils\Flash::add($e->getMessage(), 'error');
                $response->redirect('/discussions')->send();
                $this->skipRemaining();
            }
        } else {
            $topic = 'all';
        }

        switch($order) {
            case 'popular': $orderTitle = 'Popular threads'; break;
            case 'latest': $orderTitle = 'Latest threads'; break;
            case 'no-replies': $orderTitle = 'Threads with no replies'; break;
            case 'top': $orderTitle = 'Threads with the most replies'; break;
        }

        $breadcrumb = array(
            'Discussions' => '/discussions'
        );

        if ($topic != 'all') {
            if ($topic->parent) {
                $breadcrumb[$topic->parent->title] = $topic->parent->slug;
            }

            $breadcrumb[$topic->title] = '/discussions/' . $topic->slug;
        }

        $level_id = null;
        if (!empty($_GET['level'])) {
            try {
                $level = \dtw\Playground::getByID($_GET['level']);
                $level_id = $level->ID;

                if ($topic->title === 'Solutions') {
                    $topic->description = $level->title . ' level solutions';
                } else {
                    $topic->description = $level->title . ' level discussions';
                }
            } catch (\Exception $e) {
                \dtw\utils\Flash::add($e->getMessage(), 'error');
                $response->redirect('/discussions')->send();
                $this->skipRemaining();
            }
        }

        try {
            $sections = array();
            if (($order != 'noreplies' && $order != 'top') && (!isset($page) || $page == 1)) {
                try {
                    $sections['Sticky'] = $app->DtW->discussions->getList('sticky', $topic, 'latest');
                } catch (Exception $e) {
                    // Might not be any sitcky threads
                }
            }

            $threads = $app->DtW->discussions->getList($page, $topic, $order, $level_id);
            $sections[$orderTitle] = $threads;
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/discussions')->send();
            $this->skipRemaining();
        }

        if ($topic == 'all') {
            $topic = new stdClass();
            $topic->title = 'All topics';
            $topic->slug = 'all';
        }

        if (!isset($_GET['ajax'])) {
            return $app->DtW->tmpl->render('discussions/index.twig', array('breadcrumb' => $breadcrumb, 'sections' => $sections, 'topics' => $topics, 'page' => $page, 'topic' => $topic, 'order' => $order));
        } else {
            return $app->DtW->tmpl->render('discussions/threads.twig', array('threads' => $threads));
        }
    });

    /*
     * Create thread
     */
    $this->respond('GET', 's?/[:topic]?/new', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        $app->DtW->load('Discussions');

        $topics = $app->DtW->discussions->getTopics();

        $breadcrumb = array(
            'Discussions' => '/discussions',
            'New thread' => '/discussion/new'
        );

        if ($request->topic) {
            try {
                $topic = $app->DtW->discussions->getTopic($request->topic);

                if (!$topic) {
                    $topic_id = null;
                } else {
                    $topic_id = $topic->topic_id;
                }
            } catch (Exception $e) {
                \dtw\utils\Flash::add($e->getMessage(), 'error');
                $response->redirect('/discussions')->send();
                $this->skipRemaining();
            }
        } else {
            $topic = $app->DtW->discussions->getTopic('offtopic');
            $topic_id = $topic->topic_id;
        }

        // New thread form
        $form = new \dtw\utils\Form("New thread", "Create");
        $form->addField('Title', 'text', array( 'maxlength' => 128 ));
        $form->addField('Message', "markdown");

        // Topic select dropdown
        $extras = array();
        $extras['value'] = $topic_id;
        $extras['options'] = array();
        foreach($topics AS $topic) {
            $extras['options'][$topic->topic_id] = $topic->title;

            if ($topic->children && count($topic->children)) {
                foreach($topic->children AS $child) {
                    $extras['options'][$child->topic_id] = $topic->title . ' - ' . $child->title;
                }
            }
        }
        $form->addField('Topic', 'select', $extras);

        // Level select dropdown
        $extras = array();
        if (isset($_GET['level'])) {
            try {
                $level = \dtw\Playground::getByID($_GET['level']);
                $extras['value'] = $level->ID;
            } catch (\Exception $e) {
                //
            }            
        }
        $extras['options'] = array(0 => '---');
        foreach(\dtw\Playground::getLevels() AS $level) {
            $level = \dtw\Playground::getByID($level->ID);
            $extras['options'][$level->ID] = $level->title;
        }
        $form->addField('Level', 'select', $extras);

        $completed = array_values(array_map(function($l) { return intval($l->level_id); }, $app->DtW->user->levels->getCompleted()));

        $form->addField('Spoilers', 'toggle', array(
            'description' => 'Can replies contain spoilers?'
        ));

        echo $app->DtW->tmpl->render('discussions/new.twig', array('breadcrumb' => $breadcrumb, 'form' => $form, 'levels' => $completed));

        $response->send();
        $this->skipRemaining();
    });

    $this->respond('POST', 's?/[:topic]?/new', function($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        $app->DtW->load('Discussions');

        if (isset($_GET['new-thread'])) {
            try {
                $thread = $app->DtW->discussions->createThread($_POST);

                \dtw\utils\StaticCache::delete('/discussions');
            } catch (Exception $e) {
                \dtw\utils\Form::storeResponses();
                \dtw\utils\Flash::add($e->getMessage(), 'error');
                $service->back();

                $response->send();
                $this->skipRemaining();
            }
        }

        $response->redirect($thread)->send();
        $this->skipRemaining();
    });

    /*
     * View thread
     */
    $this->respond('GET', '/[:thread]', function ($request, $response, $service, $app) {
        try {
            $thread = $app->DtW->discussions->getThread($request->thread);

            $breadcrumb = array(
                'Discussions' => '/discussions'
            );

            $thread->topic = $topic = $app->DtW->discussions->getTopic($thread->topic_id);
            if ($thread->topic->parent) {
                $thread->topic->parent = $app->DtW->discussions->getTopic($thread->topic->parent);
                $breadcrumb[$thread->topic->parent->title] = '/discussions/' . $thread->topic->parent->slug;
            }

            $breadcrumb[$thread->topic->title] = '/discussions/' . $thread->topic->slug;

            $breadcrumb[$thread->title] = $thread->permalink;

            // Restrict access if user has completed level
            if (!$thread->hasAccess()) {
                throw new \Exception('Access denied');
            }

            // Show spoiler warning if user hasn't previously accepted and thread contains spoilers
            if ($thread->isSpoiler() && $app->DtW->user->isAuth() && !\dtw\user\Meta::get('discussions.spoilers')) {
                return $app->DtW->tmpl->render('discussions/spoilers.twig', array('breadcrumb' => $breadcrumb, 'thread' => $thread));
            }

            $thread->getPosts();

            // New thread form
            $form = new \dtw\utils\Form("Reply", "Post reply", array('hideTitle' => true, 'block' => false, 'draft' => 'discussion:' . $thread->id));
            if (!$thread->spoiler && ($thread->topic->slug == 'playground' || $thread->topic->parent->slug == 'playground')) {
                $form->footer = "This thread hasn't been marked as containing spoilers so please refrain from including them as to not spoil the experience for the OP";
            }
            $form->addField('Post reply', "markdown", array('placeholder' => 'Write reply...', 'name' => 'message'));

            return $app->DtW->tmpl->render('discussions/view.twig', array('breadcrumb' => $breadcrumb, 'thread' => $thread, 'form' => $form));
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/discussions')->send();
        }

        $this->skipRemaining();
    });

    /*
     * Allow access to spoilers
     */
    $this->respond('GET', '/[:thread]/spoilers', function ($request, $response, $service, $app) {
        return $app->DtW->tmpl->render('csrf-confirm.twig', array('action' => 'view threads with spoilers'));
    });
    $this->respond('POST', '/[:thread]/spoilers', function ($request, $response, $service, $app) {
        if ($app->DtW->user->isAuth()) {
            try {
                $thread = $app->DtW->discussions->getThread($request->thread);

                \dtw\user\Meta::set('discussions.spoilers', true);
            } catch (Exception $e) {
                \dtw\utils\Flash::add($e->getMessage(), 'error');
            }
        }

        $response->redirect($thread->permalink)->send();
        $this->skipRemaining();
    });

    /*
     * Reply in thread
     */
    $this->respond('POST', '/[:thread]', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        if (isset($_GET['reply'])) {
            try {
                $thread = $app->DtW->discussions->getThread($request->thread);
                $thread->addPost($_POST);

                \dtw\utils\StaticCache::delete($thread->permalink);
                \dtw\utils\StaticCache::delete('/discussions');
                \dtw\utils\Flash::add('Reply added', 'success');
            } catch (\Exception $e) {
                \dtw\utils\Form::storeResponses();
                \dtw\utils\Flash::add($e->getMessage(), 'error');
            }
        }

        $response->redirect('/discussion/' . $request->thread)->send();
        $this->skipRemaining();
    });

    /*
     * Follow thread
     */
    $this->respond('GET', '/[:thread]/[follow|unfollow:action]', function ($request, $response, $service, $app) {
        return $app->DtW->tmpl->render('csrf-confirm.twig', array('action' => ($request->action === 'follow' ? 'follow' : 'unfollow') . ' a thread'));
    });

    $this->respond('POST', '/[:thread]/[follow|unfollow:action]', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);

        try {
            $thread = $app->DtW->discussions->getThread($request->thread);
            $thread->isFollowing($request->action === 'follow');
        } catch (Exception $e) {
            var_dump($e);
            die();
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect('/discussion/' . $request->thread)->send();
        $this->skipRemaining();
    });

    /*
     * Delete thread
     */
    $this->respond('GET', '/[:thread]/delete', function ($request, $response, $service, $app) {
        return $app->DtW->tmpl->render('csrf-confirm.twig', array('action' => 'delete a thread'));
    });

    $this->respond('POST', '/[:thread]/delete', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $thread = $app->DtW->discussions->getThread($request->thread);
            $thread->delete();

            \dtw\utils\Flash::add('Thread removed', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect('/discussions')->send();
        $this->skipRemaining();
    });

    /*
     * Mark thread as unanswered
     */
    $this->respond('GET', '/[:thread]/unanswer', function ($request, $response, $service, $app) {
        return $app->DtW->tmpl->render('csrf-confirm.twig', array('action' => 'remove thread answer'));
    });

    $this->respond('POST', '/[:thread]/unanswer', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $thread = $app->DtW->discussions->getThread($request->thread);
            $thread->markAnswer(null);

            \dtw\utils\Flash::add('Thread answer removed', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect($thread->permalink)->send();
        $this->skipRemaining();
    });

    /*
     * Mark thread as spoiler
     */
    $this->respond('GET', '/[:thread]/spoiler', function ($request, $response, $service, $app) {
        return $app->DtW->tmpl->render('csrf-confirm.twig', array('action' => 'mark thread as spoiler'));
    });
    $this->respond('POST', '/[:thread]/spoiler', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $thread = $app->DtW->discussions->getThread($request->thread);
            $thread->markAsSpoiler();

            \dtw\utils\Flash::add('Thread has been marked as containing spoilers', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect($thread->permalink)->send();
        $this->skipRemaining();
    });

    /*
     * Mark thread as not spoiler
     */
    $this->respond('GET', '/[:thread]/unspoiler', function ($request, $response, $service, $app) {
        return $app->DtW->tmpl->render('csrf-confirm.twig', array('action' => 'mark thread as spoiler'));
    });
    $this->respond('POST', '/[:thread]/unspoiler', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $thread = $app->DtW->discussions->getThread($request->thread);
            $thread->markAsSpoiler(false);

            \dtw\utils\Flash::add('Thread has been unmarked as containing spoilers', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect($thread->permalink)->send();
        $this->skipRemaining();
    });

    /*
     * Mark thread as locked
     */
    $this->respond('GET', '/[:thread]/lock', function ($request, $response, $service, $app) {
        return $app->DtW->tmpl->render('csrf-confirm.twig', array('action' => 'lock thread'));
    });
    $this->respond('POST', '/[:thread]/lock', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $thread = $app->DtW->discussions->getThread($request->thread);
            $thread->lock();

            \dtw\utils\Flash::add('Thread has been locked', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect($thread->permalink)->send();
        $this->skipRemaining();
    });

    /*
     * Mark thread as unlocked
     */
    $this->respond('GET', '/[:thread]/unlock', function ($request, $response, $service, $app) {
        return $app->DtW->tmpl->render('csrf-confirm.twig', array('action' => 'unlock thread'));
    });
    $this->respond('POST', '/[:thread]/unlock', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $thread = $app->DtW->discussions->getThread($request->thread);
            $thread->lock(false);

            \dtw\utils\Flash::add('Thread has been unlocked', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect($thread->permalink)->send();
        $this->skipRemaining();
    });

    /*
     * Edit post in thread
     */
    $this->respond('GET', '/[:thread]/[:post]/edit', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $breadcrumb = array(
                'Discussions' => '/discussions'
            );

            $thread = $app->DtW->discussions->getThread($request->thread);
            $thread->topic = $topic = $app->DtW->discussions->getTopic($thread->topic_id);
            if ($thread->topic->parent) {
                $thread->topic->parent = $app->DtW->discussions->getTopic($thread->topic->parent);
                $breadcrumb[$thread->topic->parent->title] = '/discussions/' . $thread->topic->parent->slug;
            }

            $breadcrumb[$thread->topic->title] = '/discussions/' . $thread->topic->slug;
            $breadcrumb[$thread->title] = $thread->permalink;
            $breadcrumb['Edit post'] = 'edit';

            // Restrict access if user has completed level
            if (!$thread->hasAccess()) {
                throw new \Exception('Access denied');
            }

            $post = $app->DtW->discussions->getPostToEdit($thread->id, $request->post);

            // New thread form
            $form = new \dtw\utils\Form("Edit post", "Update");

            $thread->getPosts();
            $isRootPost = ($post->id == $thread->posts[0]->id);

            if ($isRootPost) {
                $form->addField('Title', "text", array(
                    'value' => $thread->title
                ));
            }

            $form->addField('Message', "markdown", array(
                'value' => $post->message->plain
            ));

            if ($isRootPost) {
                $topics = $app->DtW->discussions->getTopics();

                $extras = array();
                $extras['value'] = $thread->topic->topic_id;
                $extras['options'] = array();
                foreach($topics AS $topic) {
                    $extras['options'][$topic->topic_id] = $topic->title;

                    if ($topic->children && count($topic->children)) {
                        foreach($topic->children AS $child) {
                            $extras['options'][$child->topic_id] = $topic->title . ' - ' . $child->title;
                        }
                    }
                }
                $form->addField('Topic', 'select', $extras);

                $extras = array();
                $extras['value'] = $thread->level_id;
                $extras['options'] = array(0 => '---');
                foreach(\dtw\Playground::getLevels() AS $level) {
                    $level = \dtw\Playground::getByID($level->ID);
                    $extras['options'][$level->ID] = $level->title;
                }
                $form->addField('Level', 'select', $extras);

                $form->addField('Spoilers', 'toggle', array(
                    'value' => $thread->spoiler,
                    'description' => 'Can replies contain spoilers?'
                ));
            }

            echo $app->DtW->tmpl->render('discussions/new.twig', array('breadcrumb' => $breadcrumb, 'form' => $form));
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/discussions');
        }

        $response->send();
        $this->skipRemaining();
    });

    $this->respond('POST', '/[:thread]/[:post]/edit', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $thread = $app->DtW->discussions->getThread($request->thread);
            $post = $app->DtW->discussions->getPostToEdit($thread->id, $request->post);

            $post->edit($_POST['message']);

            // Edit title
            if (isset($_POST['title'])) {
                $thread->edit('title', $_POST['title']);
            }

            // Edit topic
            if (isset($_POST['topic'])) {
                $thread->edit('topic_id', $_POST['topic']);
            }

            // Edit spoiler
            if (isset($_POST['spoilers'])) {
                $thread->edit('spoiler', $_POST['spoilers']);
            }

            // Edit level id
            if (isset($_POST['level'])) {
                $thread->edit('level_id', $_POST['level']);
            }

            \dtw\utils\StaticCache::delete($thread->permalink);

            \dtw\utils\Flash::add('Reply updated', 'success');
            $response->redirect($thread->permalink)->send();
        } catch (Exception $e) {
            \dtw\utils\Form::storeResponses();
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $service->back();
        }

        $this->skipRemaining();
    });

    /*
     * Delete post in thread
     */
    $this->respond('GET', '/[:thread]/[:post]/delete', function ($request, $response, $service, $app) {
        return $app->DtW->tmpl->render('csrf-confirm.twig', array('action' => 'delete reply'));
    });
    $this->respond('POST', '/[:thread]/[:post]/delete', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $thread = $app->DtW->discussions->getThread($request->thread);
            $post = $app->DtW->discussions->getPostToEdit($thread->id, $request->post);
            $post->delete();

            if ($thread->answer == $post->id) {
                $thread->markAnswer(null);
            }

            $thread->clearCache();

            \dtw\utils\StaticCache::delete($thread->permalink);

            \dtw\utils\Flash::add('Reply deleted', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect($thread->permalink)->send();
        $this->skipRemaining();
    });

    /*
     * Mark post as answer for thread
     */
    $this->respond('GET', '/[:thread]/[:post]/answer', function ($request, $response, $service, $app) {
        return $app->DtW->tmpl->render('csrf-confirm.twig', array('action' => 'accept an answer'));
    });
    $this->respond('POST', '/[:thread]/[:post]/answer', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $thread = $app->DtW->discussions->getThread($request->thread);
            $thread->markAnswer($request->post);

            \dtw\utils\Flash::add('Post marked as answer', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect($thread->permalink)->send();
        $this->skipRemaining();
    });

    /*
     * Give post karma
     */
    $this->respond('GET', '/[:thread]/[:post]/vote', function ($request, $response, $service, $app) {
        return $app->DtW->tmpl->render('csrf-confirm.twig', array('action' => 'award karma'));
    });
    $this->respond('POST', '/[:thread]/[:post]/vote', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);

        try {
            $thread = $app->DtW->discussions->getThread($request->thread);
            $post = $app->DtW->discussions->getPost($thread->id, $request->post);

            $post->karma(isset($_GET['up']), isset($_GET['cancel']));

            \dtw\utils\Flash::add('Karma awarded', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        if (!isset($_POST['ajax'])) {
            $response->redirect($thread->permalink)->send();
        } else {
            \dtw\utils\Flash::load();
            $flash = \dtw\utils\Flash::get();
            if ($flash) {
                if ($flash[0]->type == 'error') {
                    $response->code(403);
                }
                echo json_encode($flash[0]);
            }
        }

        $response->send();
        $this->skipRemaining();
    });

    /*
     * Report post
     */
    $this->respond('POST', '/[:thread]/report', function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);
        $app->DtW->user->canContribute($response, $service);

        try {
            $app->DtW->discussions->report($request->thread, $_POST);
            \dtw\utils\Flash::add('Report submitted', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        if (isset($_POST['ajax'])) {
            \dtw\utils\Flash::load();
            $flash = \dtw\utils\Flash::get();
            if ($flash) {
                echo json_encode($flash[0]);
            }
        } else {
            $response->redirect('/discussion/' . $request->thread);
        }

        $response->send();
        $this->skipRemaining();

    });