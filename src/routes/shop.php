<?php
    $this->respond(function($request, $response, $service, $app) {
        $app->DtW->load('Shop');
    });

    $this->respond('GET', '', function($request, $response, $service, $app) {
        $args = array();

        $args['breadcrumb'] = array(
            'Shop' => '/shop'
        );

        $args['products'] = $app->DtW->shop->getProducts();
        $args['orders'] = $app->DtW->shop->getUserOrders();
        $args['order'] = $app->DtW->shop->order;

        return $app->DtW->tmpl->render('shop/index.twig', $args);
    });

    $this->respond('/clear', function($request, $response, $service, $app) {
        $app->DtW->shop->clearBasket();

        $response->redirect('/shop')->send();
        $this->skipRemaining();
        $response->send();
    });

    $this->respond('/success', function($request, $response, $service, $app) {
        // Get users sessions
        try {
            $type = $app->DtW->shop->orderComplete();

            if ($type === 'donation') {
                \dtw\utils\Flash::add('Thank you for the donation!! 🙏', 'success');
                $response->redirect('/donations')->send();
            } else {
                \dtw\utils\Flash::add('Order completed successfully', 'success');
                $response->redirect('/shop')->send();
            }
        } catch (\Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
            $response->redirect('/shop')->send();
        }

        $this->skipRemaining();
        $response->send();
    });

    $this->respond('POST', '/donate', function($request, $response, $service, $app) {
        try {
            $stripSession = $app->DtW->shop->donate($_POST['amount'], false);

            echo $app->DtW->tmpl->render('shop/redirect.twig', array(
                'checkout_id' => $stripSession
            ));
        } catch (\Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error', 'donation');

            $response->redirect('/shop')->send();
        }

        $this->skipRemaining();
        $response->send();
    });

    $this->respond('GET', '/checkout', function($request, $response, $service, $app) {
        $stripSession = $app->DtW->shop->getStripeSessionID();

        echo $app->DtW->tmpl->render('shop/redirect.twig', array(
            'checkout_id' => $stripSession
        ));

        die();
    });

    $this->respond('GET', '/[:productID]', function($request, $response, $service, $app) {
        $args = array();

        $args['breadcrumb'] = array(
            'Shop' => '/shop'
        );

        $args['item'] = $app->DtW->shop->getProduct($request->productID);
        $args['form'] = $app->DtW->shop->buildProductForm($args['item']);
        $args['orders'] = $app->DtW->shop->getUserOrders();
        $args['order'] = $app->DtW->shop->order;

        $args['breadcrumb'][$args['item']->name] = '/shop/' . $request->productID;

        return $app->DtW->tmpl->render('shop/product.twig', $args);

    });

    $this->respond('POST', '/[:productID]', function($request, $response, $service, $app) {
        try {
            $app->DtW->shop->addToOrder($request->productID, $_POST['sku']);

            \dtw\utils\Flash::add('Item added to basket', 'success');
        } catch (\Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }
        $service->back();
    });