<?php
    $this->respond(function ($request, $response, $service, $app) {
        $app->DtW->user->isAuth($response);

        $response->breadcrumb = array(
            'Settings' => '/settings'
        );
    });

    $this->respond('GET', '', function($request, $response, $service, $app) {
        $breadcrumb = array(
            'Settings' => '/settings'
        );

        $index = \dtw\Settings::getIndex();

        return $app->DtW->tmpl->render('includes/block-index.twig', array('breadcrumb' => $breadcrumb, 'index' => $index));
    });

    $this->respond('GET', '/oauth', function ($request, $response, $service, $app) {
        $breadcrumb = array(
            'Settings' => '/settings',
            'Social sign-in' => '/settings/oauth'
        );

        $connected = $app->DtW->user->getConnectedOauth();
        $providers = array(
            'facebook',
            'twitter',
            'github',
            'google'
        );

        echo $app->DtW->tmpl->render('settings/oauth.twig', array('breadcrumb' => $breadcrumb, 'connected' => $connected, 'providers' => $providers));

        $response->send();
        $this->skipRemaining();
    });

    $this->respond('POST', '/oauth', function ($request, $response, $service, $app) {
        $app->DtW->user->unlinkOauth($request->unlink);

        $response->redirect('oauth');
    });

    $this->respond('GET', '/userbars', function ($request, $response, $service, $app) {
        $args = array(
            'breadcrumb' => array(
                'Settings' => '/settings',
                'Userbars' => '/settings/userbars'
            )
        );

        echo $app->DtW->tmpl->render('settings/userbars.twig', $args);

        $response->send();
        $this->skipRemaining();
    }); 

    $this->respond('GET', '/followers', function ($request, $response, $service, $app) {
        $args = array(
            'breadcrumb' => array(
                'Settings' => '/settings',
                'Followers' => '/settings/followers'
            )
        );

        $args['items'] = $app->DtW->user->profile->getFollowers();

        echo $app->DtW->tmpl->render('settings/followers.twig', $args);

        $response->send();
        $this->skipRemaining();
    }); 

    $this->respond('GET', '/followed', function ($request, $response, $service, $app) {
        $args = array(
            'breadcrumb' => array(
                'Settings' => '/settings',
                'Followed' => '/settings/followed'
            )
        );

        $args['items'] = $app->DtW->user->profile->getFollowing();

        echo $app->DtW->tmpl->render('settings/followers.twig', $args);

        $response->send();
        $this->skipRemaining();
    }); 

    $this->respond('GET', '/[s:page]', function ($request, $response, $service, $app) {
        $page = \dtw\Settings::getPage($request->page);

        if (!$page) {
            $this->abort(404);
            die();
        }

        return $app->DtW->tmpl->render('form.twig', array(
            'breadcrumb' => $page['breadcrumb'],
            'form' => $page['form'],
            'hideBack' => true,
            'hideTitle' => true
        ));
    });

    // Profile settings
    $this->respond('POST', '/profile', function($request, $response, $service, $app) {
        try {
            $app->DtW->user->update($_POST);
            \dtw\utils\Flash::add('Profile updated', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect('profile');
    });

    // Privacy settings
    $this->respond('POST', '/privacy', function($request, $response, $service, $app) {
        try {
            $app->DtW->user->settings->save($_POST);
            \dtw\utils\Flash::add('Privacy settings updated', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect('privacy');
    });

    // Email settings
    $this->respond('POST', '/email', function ($request, $response, $service, $app) {
        try {
            $app->DtW->user->changeEmail($_POST['current-password'], $_POST['email'], $_POST['re-enter-email']);
            \dtw\utils\Flash::add('Email updated', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect('email');
    });

    $this->respond('POST', '/password', function ($request, $response, $service, $app) {
        try {
            $app->DtW->user->changePassword($_POST['current-password'], $_POST['new-password'], $_POST['re-enter-new-password']);
            \dtw\utils\Flash::add('Password updated', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect('password');
    });

    $this->respond('POST', '/2fa', function ($request, $response, $service, $app) {
        if (!isset($_POST['code']) || empty($_POST['code'])) {
            \dtw\utils\Flash::add('Code missing', 'error');
        } else {
            $code = $_POST['code'];

            // Disable 2FA if enabled
            if ($app->DtW->user->twoFactor) {
                $secret = $app->DtW->user->twoFactor;
        
                $twoFactor = new \dtw\utils\TwoFactor();
                $checkResult = $twoFactor->verifyCode($secret, $code);
                if ($checkResult) {
                    if ($app->DtW->user->disable2FA()) {
                        \dtw\utils\Flash::add('2FA disabled', 'success');
                        unset($_SESSION['settings.2fa']);
                    } else {
                        \dtw\utils\Flash::add('There was an error updating your settings, try again', 'error');
                    }
                } else {
                    \dtw\utils\Flash::add('Invalid code, try again', 'error');
                }

            } else {
                $secret = $_SESSION['settings.2fa'];
        
                $twoFactor = new \dtw\utils\TwoFactor();
                $checkResult = $twoFactor->verifyCode($secret, $code);
                if ($checkResult) {
                    if ($app->DtW->user->enable2FA($secret)) {
                        \dtw\utils\Flash::add('2FA setup correctly', 'success');
                        unset($_SESSION['settings.2fa']);
                    } else {
                        \dtw\utils\Flash::add('There was an error updating your settings, try again', 'error');
                    }
                } else {
                    \dtw\utils\Flash::add('Invalid code, try again', 'error');
                }
            }
        }

        $response->redirect('2fa');
    });

    $this->respond('POST', '/username', function ($request, $response, $service, $app) {
        try {
            $app->DtW->user->changeUsername($_POST['new-username']);
            \dtw\utils\Flash::add('Username updated', 'success');
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect('username');
    });

    $this->respond('POST', '/delete', function ($request, $response, $service, $app) {
        try {
            if (!$_POST['confirmation']) {
                throw new \Exception('Confirmation needed');
            }

            $app->DtW->user->deleteAccount($_POST['password']);
            \dtw\utils\Flash::add('Account deleted', 'success');

            $response->redirect('/');
            return;
        } catch (Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect('delete');
    });

    $this->respond('POST', '/notifications', function($request, $response, $service, $app) {
        try {
            $app->DtW->user->settings->save($_POST);
            \dtw\utils\Flash::add('Settings updated', 'success');
        } catch (\Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect('notifications');
    });

    $this->respond('POST', '/emails', function($request, $response, $service, $app) {
        try {
            $app->DtW->user->settings->save($_POST);
            \dtw\utils\Flash::add('Settings updated', 'success');
        } catch (\Exception $e) {
            \dtw\utils\Flash::add($e->getMessage(), 'error');
        }

        $response->redirect('emails');
    });