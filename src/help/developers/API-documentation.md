Most requests require a valid API key. Keys can be generated and managed [here](/api).

## Profiles

Request basic user details:

```
/api/1/user/profile?key={key}&id=flabbyrabbit
```

Request all user details:

```
/api/1/user/profile?key={key}&id=flabbyrabbit&extras=medals,levels
```

## Discussions
### Threads

Request latest threads:

```
/api/1/discussions/threads?key={key}
```

Request sorted threads:

```
/api/1/discussions/threads?key={key}&order={latest|top|popular|no-replies}
```

## Medals

Get full list of medals:

```
/api/1/medals/list?key={key}
```

Lookup a single medal:

```
/api/1/medals/medal?key={key}&id={medalID}
```