We have compiled a small guide on how to start your journey and included some tips to help you along.

## First steps
* Complete the To-Do list on the site. If you are reading this then you have already completed one step, so congrats! This is here to ease you into the site and get you started.
* Post an introduction on our forums. Although this is not compulsory, it will give us a chance to say hello and to get to know you.
* Start with the challenges! After all, this is what the site is about :)

## How to analyse challenges
* There is no one true way to analyse challenges and analysis depends on the type of challenge you are up against. Here are some general pointers for each type of challenge currently offered on Defend The Web
* Intro – View the source! This should be your first port of call for any challenge, you never know what the source code might hold if you study it carefully. Other than this look into intercepting your own requests with some intercepting proxy. Don't know what we're talking about?  To Google!
* JavaScript – View the source! (Anyone see a pattern emerging here?) JavaScript protections are client-side, so you can modify them to your heart's content...
* SQLi – Database hacking right here. We suggest learning about SQL and understanding how user input can affect poorly-sanitised SQL queries
* Coding – Some programming knowledge needed here. Just know that if you decide to gain the data or send the solution automatically, you will need to have your code auth to the site, so look into sending cookies with your requests, and watch out for that pesky CSRF-protection on the forms... could cause a real headache
* Crypt – Good old-fashioned code-cracking. Although there are tools around to help with these, we suggest trying them manually. You will learn a lot more and it may come in handy elsewhere :)
* Real – Now we get to the fun stuff: Web-Hacking.  Your best resource for this without a doubt is OWASP (To the Google!). These challenges are likely more involved than those in other sections. Take your time and study everything (Including viewing the source...  sorry. Couldn't resist!)

## Common mistakes
* Copy/paste – Avoid this if you can, or at very least be careful when copying and pasting data into a form. There is usually leading or trailing whitespace which has caused even experienced challengers a headache from time to time
* Simulated challenges - Not all missions are going to work how they do in the wild, for example SQL injections. Some of these are simulated. If they are simulated don't expect all your queries to work. They are hard coded to only except certain input. If they are live don't expect all queries to work either as many have been filtered to only allow accepted input.
* Some challenges like Real 6 are not simulated but have been known to malfunction from time to time. If you believe this is the case, politely ask in the appropriate topic for that challenge or contact an administrator
* If your solution is not being accepted and you are sure it is correct, reload the page and try again. Chances are your csrf token was a little messed up (This happens if you have the same page open in multiple tabs

## Where to go to learn
* To the Google!  Seriously. Go there. Learn.
* A crash course in challenge solving is available here, courtesy of Caesum: http://www.caesum.com/handbook/contents.htm

## How to use the forum well
* Firstly please read topics in the forum before starting your own. Chances are your question has been already answered.
* If this is not the case, then start your own topic, however here comes the tricky part: Let people know which part of the challenge you are stuck on and why but please do NOT give away or spoil any part of the challenge for others. 
* Use spoiler tags if you have to if you want to include challenge-specific information but we would prefer no spoilers at all if possible.
* The person opening a topic is responsible for closing that topic. Once your question has been answered, please close your topic!

## How to use the PM system without people getting annoyed
* [Head to our sticky thread here](/discussion/4893-help-via-pm-a-guide)

## What to do if still stuck
* Many of us are available in the DtW Matrix chatroom if you want one-on-one communication or real-time communication in a group.
* Head to [chat](/chat) or setup your Matrix client using this [handy guide](/help/how-to/using-chat-and-matrix).
* Take a break! Leave it for the night, move on to another, continue doing your knitting or something. When you come back to it you may have some new ideas.

## What can I not do?
* Please do not bruteforce the server. Some challenges (for example JavaScript) can be bruteforced locally if you feel you need to go that way, but any forms pointing to the Defend The Web server or any Defend The Web URL should NOT be bruteforced.
* No posting solutions – Either here or elsewhere – It is just bad form. These challenges are here for you to solve, so take your time and try hard.
* Don't use solutions found online – Where is the fun in that? Besides you'll be missing out on the wonderful feeling of solving a challenge which you worked extra hard on.
* No spamming in the forums – Seriously. This one just pisses us off.
* Don't be that guy we all roll our eyes at  - Be nice. Be polite, join the community. You'll fit right in here.

## What is WeChall and how is Defend The Web affiliated?
* Wechall is a global ranking for over 50 challenge sites. Defend The Web is one of the sites which is part of this network and you can link your Defend The Web account to your WeChall account in order to earn points and ranks on WeChall. Sign up at http://wechall.net

Have Fun! :)
