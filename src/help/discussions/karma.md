Karma is a way to rate users posts within the forum. The karma rating is displayed as a number on the left of each forum post. All users who have completed a level are allowed to upvote posts but only users with Bronze moderator status can down vote.

You advance your moderator status by completing levels and being active within the forum, for more, please view our [help page on reputation and privileges](https://defendtheweb.net/help/reputation-and-privileges/what-is-reputation)
