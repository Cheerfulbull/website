<?php
    namespace dtw\utils;

    class Videos {

        public static function registerHooks() {
            \dtw\DtW::$api->registerHook('videos', 'watch', function($params) {
                if (!isset($_POST['vid'])) {
                    throw new \Exception('Invalid ID');
                }

                return \dtw\utils\Videos::countView($_POST['vid'], $_POST['source']);
            });
        }

        public static function countView($vid, $source) {
            if (!in_array($source, array('youtube'))) {
                throw new \Exception('Invalid source');
            }

            if (!isset($_SESSION['videos'])) {
                $_SESSION['videos'] = array();
            } else if (in_array($vid, $_SESSION['videos'])) {
                return true;
            }

            $stmt = \dtw\DtW::$db->prepare('
                INSERT INTO videos (`id`, `source`, `views`)
                VALUES (:vid, :source, 1)
                ON DUPLICATE KEY UPDATE `views` = `views` + 1
            ');
            $stmt->execute(array(
                ':vid' => $vid,
                ':source' => $source
            ));

            $_SESSION['videos'][$vid] = true;

            return true;
        }

    }