<?php
    namespace dtw\utils;

    class Tmpl {
        public function __construct() {
            $DtW = \dtw\DtW::getInstance();
            $loader = new \Twig_Loader_Filesystem(__DIR__ . "/../../templates/");
            $this->twig = new \Twig_Environment($loader, array(
                'auto_reload' => \dtw\DtW::$config->get('debug'),
                'cache' => __DIR__ . "/../../templates/cache"
            ));

            // Include text truncation functions
            $this->twig->addExtension(new \Dzango\Twig\Extension\Truncate());

            $queue = new \Twig_SimpleFunction('queue', function($name, $values) {
                $args = func_get_args();


                $globals = $this->twig->getGlobals();
                if (array_key_exists($name, $globals)) {
                    $global = $globals[$name];
                } else {
                    $global = Array();
                }

                for ($i = 1; $i < func_num_args(); $i++) {
                    $path = $args[$i];

                    // Check if it's a static resource
                    if ($path[0] == '/') {
                        $path = \dtw\DtW::$config->get('site.static') . $path;
                    }

                    array_push($global, $path);
                }

                $this->twig->addGlobal($name, $global);
            });
            $this->twig->addFunction($queue);

            // Add twig function to get log
            $this->twig->addFunction(new \Twig_SimpleFunction('getLog', function() {
                return \dtw\DtW::$log->getLog();
            }));

            // Add twig function to determine active sidebar item
            $this->twig->addFunction(new \Twig_SimpleFunction('sidebarSelected', function($item, $breadcrumb) {
                if (!$breadcrumb || !count($breadcrumb)) {
                    return false;
                }

                foreach($breadcrumb AS $crumb) {
                    if ($item['url'] && $crumb == $item['url']) {
                        return true;
                    }

                    if ($item['children']) {
                        foreach($item['children'] AS $child) {
                            if ($crumb == $child['url']) {
                                return true;
                            }
                        }
                    }
                }

                return false;
            }));

            // Add twig function to get flashes
            $this->twig->addFunction(new \Twig_SimpleFunction('getFlashes', function($group = null) {
                return \dtw\utils\Flash::get($group);
            }));

            // Add twig function to get CSRF token
            $this->twig->addFunction(new \Twig_SimpleFunction('getTokenField', function() {
                $token = \dtw\utils\CSRF::generate();
                return new \Twig_Markup("<input type='hidden' name='token' value='{$token}'/>", 'UTF-8');
            }));

            // Add twig function to get full image tag
            $this->twig->addFunction(new \Twig_SimpleFunction('getImage', function($file, $size, $class='') {
                $DtW = \dtw\DtW::getInstance();
                $DtW->load('Images');

                try {
                    $width = $DtW->images->getSize($size)[0];
                    $src = $DtW->images->get($file, $size, true);
                } catch (\Exception $e) {
                    $src = $DtW->images->getDefault($size);
                }

                $html = sprintf("<img src='%s' data-lazy-image-size='%s' width='%d' class='%s' />", $src, $size, $width, $class);

                return $html;
            }));

            // Add twig function to get image src
            $this->twig->addFunction(new \Twig_SimpleFunction('getImageSrc', function($file, $size) {
                $DtW = \dtw\DtW::getInstance();
                $DtW->load('Images');

                try {
                    return $DtW->images->get($file, $size);
                } catch (\Exception $e) {
                    return $DtW->images->getDefault($size);
                }
            }));

            // Add twig function to handle plurals
            $this->twig->addFunction(new \Twig_SimpleFunction('pluralize', function($count, $one, $many, $none = null) {
                // Make sure $count is a numeric value
                if (!is_numeric($count))
                    return 'N/A';

                // If the option for $none is null, use the option for $many
                if ($none === null)
                    $none = $many;

                // Handle 0
                switch( $count ) {
                    case 0:
                        $string = $none;
                        break;
                    case 1:
                        $string = $one;
                        break;
                    default:
                        $string = $many;
                        break;
                }

                // Return the result
                return sprintf( $string, $count );
            }));

            // Time ago
            $this->twig->addFilter(new \Twig_SimpleFilter('ago', function($datetime, $since = null, $ago = true, $short = false) {
                if (!is_int($datetime)) {
                    $datetime = strtotime($datetime);
                }

                if ($since) {
                    $time = strtotime($since) - $datetime; 
                } else {
                    $time = time() - $datetime; 
                }

                if ($time <= 0) {
                    if ($short) {
                        return 'secs' . ($ago ? ' ago' : '');
                    } else {
                        return 'seconds' . ($ago ? ' ago' : '');
                    }
                }

                $date = date('d/m/Y H:i (T)', $datetime);

                $units = array (
                    31536000 => 'year',
                    2592000 => 'month',
                    604800 => 'week',
                    86400 => 'day',
                    3600 => 'hour',
                    60 => 'minute',
                    1 => 'second'
                );

                if ($short) {
                    $units = array (
                        31536000 => 'yr',
                        2592000 => 'mth',
                        604800 => 'wk',
                        86400 => 'dy',
                        3600 => 'hr',
                        60 => 'min',
                        1 => 'sec'
                    );
                }

                foreach ($units as $unit => $val) {
                    if ($time < $unit) continue;
                    $numberOfUnits = floor($time / $unit);

                    $response = '';
                    if ($ago && $val == 'second') {
                        if ($short) {
                            $response = 'secs';    
                        } else {
                            $response = 'seconds';
                        }
                    } else {
                        $response = (($numberOfUnits>1) ? $numberOfUnits : ($val == 'hour' ? 'an' : 'a')) . ' ' . $val . (($numberOfUnits>1) ? 's' : '');
                    }

                    if ($ago) {
                        $response .= ' ago';
                        return new \Twig_Markup("<span title='{$date}'>{$response}</span>", 'UTF-8');
                    }

                    return new \Twig_Markup($response, 'UTF-8');
                }
            }));

            // Time ago
            $this->twig->addFilter(new \Twig_SimpleFilter('date', function($datetime, $format = null) {
                if (!is_int($datetime)) {
                    $datetime = strtotime($datetime);
                }

                if (!$format) {
                    if (date('Y') === date('Y', $datetime)) {
                        $format = 'j M';
                    } else {
                        $format = 'j M Y';
                    }
                }

                $date = date('d/m/Y H:i', $datetime);
                $string = date($format, $datetime);

                return new \Twig_Markup("<span title='{$date}'>{$string}</span>", 'UTF-8');
            }));


            // Duration
            $this->twig->addFilter(new \Twig_SimpleFilter('duration', function($seconds) {
                $units = array (
                    31536000 => 'year',
                    2592000 => 'month',
                    604800 => 'week',
                    86400 => 'day',
                    3600 => 'hour',
                    60 => 'minute',
                    1 => 'second'
                );

                foreach ($units as $unit => $val) {
                    if ($seconds < $unit) continue;
                    $numberOfUnits = floor($seconds / $unit);

                    return (($numberOfUnits>1) ? $numberOfUnits : 'a') . ' ' . $val . (($numberOfUnits>1) ? 's' : '');
                }
            }));


            // Ordinal suffic
            $this->twig->addFilter(new \Twig_SimpleFilter('ordinal', function($number) {
                $ends = array('th','st','nd','rd','th','th','th','th','th','th');
                if ((($number % 100) >= 11) && (($number%100) <= 13))
                    return 'th';
                else
                    return $ends[$number % 10];
            }));

            // Load user details
            $this->twig->addFunction(new \Twig_SimpleFunction('getUser', function($user_id) {
                try {
                    return \dtw\Users::getUser($user_id);
                } catch (\Exception $e) {
                    return null;
                }
            }));

            // Load medal details
            $this->twig->addFunction(new \Twig_SimpleFunction('getMedal', function($medal_id) {
                try {
                    return \dtw\Medals::getMedal(intval($medal_id));
                } catch (\Exception $e) {
                    return null;
                }
            }));

            // Load discussion details
            $this->twig->addFunction(new \Twig_SimpleFunction('getDiscussion', function($thread_id) {
                try {
                    return new \dtw\discussion\Thread($thread_id);
                } catch (\Exception $e) {
                    return null;
                }
            }));

            // Load article details
            $this->twig->addFunction(new \Twig_SimpleFunction('getArticle', function($article_id) {
                try {
                    return \dtw\Articles::getArticle($article_id);
                } catch (\Exception $e) {
                    return null;
                }
            }));

            // Get level count
            $this->twig->addFunction(new \Twig_SimpleFunction('getLevelCount', function() {
                return \dtw\Playground::getLevelCount();
            }));

            // Load level details
            $this->twig->addFunction(new \Twig_SimpleFunction('getLevel', function($level_id) {
                $DtW = \dtw\DtW::getInstance();

                try {
                    return \dtw\Playground::getByID($level_id);
                } catch (\Exception $e) {
                    return null;
                }
            }));

            // Get users next level
            $this->twig->addFunction(new \Twig_SimpleFunction('getNextLevel', function() {
                return \dtw\Playground::getNextLevel();
            }));

            // Get random shop image
            $this->twig->addFunction(new \Twig_SimpleFunction('getRandomShopImage', function() {
                $products = \dtw\DtW::$config->get('products');

                return $products[rand(0, count($products) - 1)]['image'];
            }));

            // Get random advert
            $this->twig->addFunction(new \Twig_SimpleFunction('getRandomAdvert', function() {
                $type = mt_rand(1,2) / 1;

                switch($type) {
                    case 1:
                        $type = 'amazon';
                        $products = \dtw\DtW::$config->get('amazon');
                        break;
                    default:
                        $products = \dtw\DtW::$config->get('products');
                        $type = 'shop';
                }

                $product = $products[rand(0, count($products) - 1)];
                $product['type'] = $type;
                return $product;
            }));

            // Get random shop image
            $this->twig->addFunction(new \Twig_SimpleFunction('getSearchForm', function($term = null) {
                return\dtw\Search::getForm($term);
            }));

            $navigation = array();
            $navigation['sidebar'] = $DtW->getSidebar();

            // Add template engine globalS
            $site = array();
            $site['title'] = \dtw\DtW::$config->get('site.title', 'Defend the Web');
            $site['domain'] = \dtw\DtW::$config->get('site.domain');
            $site['static'] = \dtw\DtW::$config->get('site.static');
            $site['sentry'] = \dtw\DtW::$config->get('sentry');
            if (\dtw\DtW::$config->get('websocket.server')) {
                $site['websocket'] = \dtw\DtW::$config->get('websocket.server');
            }
            $site['version'] = \dtw\DtW::$version;
            $this->twig->addGlobal('site', $site);
            $this->twig->addGlobal('navigation', $navigation);
        }

        public function render($template, $data) {
            return $this->twig->render($template, $data);
        }

        public function clearAll() {
            $cache = $this->twig->getCache();
            if (is_string($cache)) {
                foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($cache), \RecursiveIteratorIterator::LEAVES_ONLY) as $file) {
                    if ($file->isFile()) {
                        @unlink($file->getPathname());
                    }
                }
            }
        }
    }
?>