<?php
    namespace dtw\utils;

    class Purifier {
        public function __construct() {
            $config = \HTMLPurifier_Config::createDefault();
            $config->set('HTML.SafeIframe', true);
            $config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|embed\.spotify\.com\/)%'); //allow YouTube and Vimeo
            $config->set('HTML.Nofollow', true);
            $config->set('HTML.TargetBlank', true);

            // Allow custom data tags
            $def = $config->getHTMLDefinition(true);
            $def->addAttribute('a', 'data-youtube', 'Text');

            $this->purifier = new \HTMLPurifier($config);
        }

        public function purify($html) {
            return $this->purifier->purify($html);
        }
    }
?>