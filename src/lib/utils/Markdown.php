<?php
    namespace dtw\utils;

    class Markdown {

        static public function registerHooks() {
            \dtw\DtW::$api->registerHook('markdown', 'parse', function($params) {
                if (!isset($_POST['markup'])) {
                    return '';
                }

                return \dtw\utils\Markdown::parse($_POST['markup'], true);
            });
        }

        static public function parse($markup, $purify = false, $mentions = true) {
            $markup = Markdown::convertBBCode($markup);

            $markup = preg_replace('/\n/', "  \n", $markup); // Add spaces to the end of lines to force new line

            $tempfile = '/tmp/dtw-markdown-' . uniqid();

            // Replace user mentions
            $markup = Markdown::parseLevels($markup);
            $markup = Markdown::parseArticles($markup);
            $markup = Markdown::parseMentions($markup, true);

            $markup = Markdown::convertSpoiler($markup);

            // Push markup to temporary file
            $fp = fopen($tempfile, 'w');
            fwrite($fp, $markup);
            fclose($fp);

            // Run markdown parser
            $cmd = \dtw\DtW::$config->get('discount');
            $html = shell_exec($cmd . ' -f nostyle -f footnotes -f autolink -f fencedcode -f tabstop ' . $tempfile);

            // Remove temporary file
            unlink($tempfile);

            $html = Markdown::convertYoutube($html);
            $html = Markdown::convertSpotify($html);

            if ($purify) {
                $purifier = new \dtw\utils\Purifier();
                $html = $purifier->purify($html);
            }

            // Convert asciinema tags
            $html = Markdown::convertAsciinema($html);

            return $html;
        }

        static public function parseMentions($string, $replace = false) {
            $regex = "/(?:(?<=\s)|^)@(\w*[0-9A-Za-z_.-]+\w*)/";
            preg_match_all($regex, $string, $matches);

            $mentions = array();

            foreach ($matches[1] AS $username) {
                try {
                    $user = \dtw\Users::getUser($username);
                    $mentions[$username] = $user;
                } catch (\Exception $e) {
                    // Invalid username
                }
            }

            if (!$replace) {
                return $mentions;
            }

            $string = preg_replace_callback($regex, function($match) use ($mentions) {
                if (array_key_exists($match[1], $mentions)) {
                    return sprintf('[%s](/profile/%s)', $match[0], $match[1]);
                } else {
                    return $match[0];
                }
            }, $string);

            return $string;
        }

        static public function parseArticles($string) {
            $regex = "/@article:(\d+)/";
            preg_match_all($regex, $string, $matches);

            $articles = array();

            foreach ($matches[1] AS $articleID) {
                try {
                    $article = \dtw\Articles::getArticle($articleID);
                    $articles[$articleID] = $article;
                } catch (\Exception $e) {
                    echo 'Invalid article ID';
                }
            }

            $string = preg_replace_callback($regex, function($match) use ($articles) {
                if (array_key_exists($match[1], $articles)) {
                    $article = $articles[$match[1]];
                    return sprintf('[%s](%s)', $article->title, $article->permalink);
                } else {
                    return $match[0];
                }
            }, $string);

            return $string;
        }

        static public function parseLevels($string) {
            $regex = "/@level:(\d+)/";
            preg_match_all($regex, $string, $matches);

            $levels = array();

            foreach ($matches[1] AS $levelID) {
                try {
                    $level = \dtw\Playground::getById($levelID);
                    $levels[$levelID] = $level;
                } catch (\Exception $e) {
                    // Invalid username
                }
            }

            $string = preg_replace_callback($regex, function($match) use ($levels) {
                if (array_key_exists($match[1], $levels)) {
                    $level = $levels[$match[1]];
                    return sprintf('[%s](/playground/%s)', $level->title, $level->slug);
                } else {
                    return $match[0];
                }
            }, $string);

            return $string;
        }

        static private function convertBBCode($markup) {
            $matches = array();
            if(preg_match_all ( "/\[list.*\].*\[\/list.*\]/Ui", $markup, $matches ) > 0) {
                $matches = $matches[0];
                foreach($matches as $match) {
                    $replace = $match;

                    $replace = preg_replace('/\[list:.*\](.*)\[\/list:.*\]/Ui', '<ul>\1</ul>', $replace);
                    $replace = preg_replace('/\[list=1:.*\](.*)\[\/list:.*\]/Ui', '<ol>\1</ol>', $replace);
                    $replace = preg_replace('/\[list=a:.*\](.*)\[\/list:.*\]/Ui', '<ol type="a">\1</ol>', $replace);
                    $replace = preg_replace('/\[\*.*\](.*)<br>/Ui', '<li>\1</li>', $replace);
                    $replace = preg_replace('/<br>/Ui', '</li><li>', $replace);
                    $replace = preg_replace('/l\>\<\/li\>/Ui', 'l>', $replace);
                    $replace = preg_replace('/\<li\>\<\//Ui', '</', $replace);
                    $replace = str_replace('<li><li>', '<li>', $replace);

                    $markup = str_replace($match, $replace, $markup);
                }
            }

            $search = array (
                "/\[h1\](.*)\[\/h1\]/Ui", //heading 1
                "/\[h2\](.*)\[\/h2\]/Ui", //heading 2
                "/\[h3\](.*)\[\/h3\]/Ui", //heading 3
                "/\[b\](.*)\[\/b\]/Ui", //bold
                "/\[i\](.*)\[\/i\]/Ui", //italic
                "/\[u\](.*)\[\/u\]/Ui", //underline
                "/\[img\](.*)\[\/img\]/Ui", //images
                "/\[code\]([\s\S]*)\[\/code\]/Ui", //code-blocks
                "/\[url\](.*)\[\/url\]/Ui", //links
                "/\[url=(.*)\](.*)\[\/url\]/Ui", //links with names
                "/\[color.*\](.*)\[\/color.*\]/Ui", //color
                "/\[size.*\](.*)\[\/size.*\]/Ui", //color
                "/\[quote]([\s\S]*)\[\/quote]*\]/si",
                "/\[center]([\s\S]*)\[\/center]*\]/si",
                "/->(.*?)<-/si",
                "/\[spoiler]([\s\S]*)\[\/spoiler]*\]/si",
                "/\[youtube]([\w\-]{10,12})\[\/youtube]/i",
                "/\[youtube]((?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch?.*?v=))([\w\-]{10,12}))\[\/youtube]/i"
            );
            $replace = array (
                '#\1',
                '##\1',
                '###\1',
                '**\1**',
                '*\1*',
                '\1',
                '![Image](\1)', //images to links
                '```\1```',
                '[\1](\1)',
                '[\2](\1)',
                '\1',
                '\1',
                '>\1',
                '<center>\1</center>',
                '<center>\1</center>',
                '<!- \1 -!>',
                'https://youtube.com/watch?v=\1',
                'https://youtube.com/watch?v=\2'
            );
            $count = 0;
            $markup = preg_replace($search, $replace, $markup, -1, $count);
            if ($count == 0) {
                return $markup;
            } else {
                return Markdown::convertBBCode($markup);
            }
        }

        static private function convertSpoiler($markup) {
            $search = '/<!- *(.*?) *-!>/si';
            $markup = preg_replace_callback($search, function($matches) {
                $replace = '';
                foreach(explode("\n", $matches[1]) AS $line) {
                    $replace .= '>> ' . $line . "\n";
                }
                return $replace;
            }, $markup);

            return $markup;
        }

        static private function convertYoutube($markup) {
            $search = '#(?:<a href=[\'"][^\'"]*[\'"]>)?((?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch?.*?v=))([\w\-]{10,12}))(?:</a>)?#';
            $replace = '<a class="video video--placeholder" href="https://youtube.com/watch?v=$2" target="_blank" data-youtube="$2" style="background-image: url(\'https://img.youtube.com/vi/$2/maxresdefault.jpg\')"></a>';
            $markup = preg_replace($search, $replace, $markup);

            return $markup;
        }

        static private function convertSpotify($markup) {
            $search = '/<a href="https:\/\/open\.spotify\.com\/(\S*)">.*<\/a>/U';
            $markup = preg_replace_callback($search, function($matches) {
                $uri = 'spotify/' . $matches[1];
                $uri = str_replace('/', ':', $uri);
                $uri = urlencode($uri);
                return "<iframe sandbox src=\"https://embed.spotify.com/?uri={$uri}\" width=\"100%\" height=\"300\" frameborder=\"0\" allowtransparency=\"true\"></iframe>";
            }, $markup);

            return $markup;
        }

        static private function convertAsciinema($html) {
            $search = '/<a href="https:\/\/asciinema\.org\/a\/([0-9]*)" [^>]+>.*<\/a>/U';
            $replace = '<script src="https://asciinema.org/a/$1.js" id="asciicast-$1" async></script>';
            $html = preg_replace($search, $replace, $html);

            return $html;
        }
    }
?>