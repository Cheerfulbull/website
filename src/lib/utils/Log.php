<?php
    namespace dtw\utils;

    class Log {
        public function __construct() {
            $this->path = realpath(dirname($_SERVER['DOCUMENT_ROOT'], 2) . '/logs');

            if (!file_exists($this->path)) {
                mkdir($this->path, 0770, true);
            }

            $this->path .= '/app.log';

            $this->log = array();
            $this->logger = new \Monolog\Logger('name');
            // $this->logger->pushHandler(new \Monolog\Handler\TestHandler());
            $this->logger->pushHandler(new \Monolog\Handler\RotatingFileHandler($this->path, 7, \Monolog\Logger::WARNING));

            \dtw\DtW::$api->registerHook('log', 'error', [$this, 'handleLog']);
            \dtw\DtW::$api->registerHook('log', 'info', [$this, 'handleLog']);
        }

        public function handleLog($data) {
            $allowed = array('msg', 'url', 'line');
            $log = array();

            foreach($allowed AS $key) {            
                if ($data[$key]) {
                    $log[$key] = $data[$key];
                }
            }

            $this->info('api.log', $log);
        }

        public function info($msg, $data = null) {
            if (array_key_exists('source', $data) && $data['source'] == 'redis') return;

            if ($data) {
                array_push($this->log, implode($data, ' | '));
            }
            return $this->logger->addInfo($msg, $data);
        }

        public function error($msg, $data = null) {
            if ($data) {
                array_push($this->log, implode($data, ' | '));
            }
            return $this->logger->addError($msg, $data);
        }

        public function getLog($full = false) {
            if ($full) {
                $file = realpath(dirname($_SERVER['DOCUMENT_ROOT'], 2) . '/logs/app-' . date('Y-m-d') . '.log');
                if (file_exists($file)) {
                    return file_get_contents($file);
                }
            } else {
                if (!\dtw\DtW::$config->get('debug') || !\dtw\DtW::$config->get('showLog')) {
                    return;
                }

                return $this->log;
            }
        }
    }
?>