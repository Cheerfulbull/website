<?php
    namespace dtw\utils;

    class Database extends \PDO {
        private $connected = false;

        public function __construct() {

        }

        public function prepare($statement, $options = NULL) {
            $this->connect();

            if (!$options) {
                $options = array();
            }

            return parent::prepare($statement, $options);
        }

        public function beginTransaction() {
            $this->connect();

            return parent::beginTransaction();
        }

        private function connect() {
            if ($this->connected) return;

            $this->connected = true;
            \dtw\DtW::$log->info(__CLASS__, array('msg' => "Connecting to DB"));

            $trace = debug_backtrace();
            \dtw\DtW::$log->info(__CLASS__, array('msg' => $trace[1]['file'] . ':' . $trace[1]['line']));
            // die();

            $config = \dtw\DtW::$config->get('db');
            try {
                $dsn = "{$config['driver']}:host={$config['host']}";
                $dsn .= (!empty($config['port'])) ? ';port=' . $config['port'] : '';
                $dsn .= ";dbname={$config['database']}";

                parent::__construct($dsn, $config['username'], $config['password']);

                $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                $this->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
                $this->setAttribute(\PDO::MYSQL_ATTR_FOUND_ROWS, true);

                parent::exec("SET time_zone='+00:00';");
            } catch(\PDOException $e) {
                echo "No DB connection";
                \dtw\DtW::$log->error(__CLASS__, array('msg' => $e->getMessage()));
                die();
            }
        }
    }
?>