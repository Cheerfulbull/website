<?php
    namespace dtw\moderation;

    class TicketsQueue {

        public function __construct() {
            if (!\dtw\DtW::getInstance()->user->hasPrivilege('tickets')) {
                throw new \Exception('Invalid permissions');
            }

            $this->queues = array();
            $this->queues['open'] = array(
                'title' => 'Open tickets',
                'description' => 'Review open tickets',
            );
            $this->queues['spam'] = array(
                'title' => 'Spam tickets',
                'description' => 'Review tickets marked as spam',
            );
            $this->queues['resolved'] = array(
                'title' => 'Resolved tickets',
                'description' => 'Review tickets marked as resolved',
            );
            $this->queues['closed'] = array(
                'title' => 'Closed tickets',
                'description' => 'Review tickets marked as closed',
            );
        }

        public function getQueues() {
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `status`, count(*) AS `status_count`
                FROM `tickets`
                GROUP BY `status`'); 
            $stmt->execute();
            $counts = $stmt->fetchAll();

            foreach($counts AS $count) {
                if (array_key_exists($count->status, $this->queues)) {
                    $this->queues[$count->status]['count'] = $count->status_count;
                }
            }

            foreach($this->queues AS &$queue) {
                $queue['access'] = \dtw\DtW::getInstance()->user->hasPrivilege('tickets');
            }

            return $this->queues;
        }

    }
