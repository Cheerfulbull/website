<?php
    namespace dtw;

    class Admin {
        private $list;

        public function __construct() {
            if (!\dtw\DtW::getInstance()->user->hasPrivilege('admin')) {
                throw new \Exception('Invalid permissions');
            }
        }

        public function flushAll() {
            // Clear redis
            \dtw\DtW::$redis->flushall();

            // Clear Twig cache
            (new utils\Tmpl)->clearAll();

            // Clear static cache
            \dtw\utils\StaticCache::clearAll();
        }

        public function getStats() {
            $stats = array();


            // User stats
            $stats['users'] = array();

            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `total` FROM `users`'); 
            $stmt->execute();
            $row = $stmt->fetch();
            $stats['users']['total'] = $row->total;

            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `total` FROM `user_activity` WHERE `active` >= NOW() - INTERVAL 1 WEEK'); 
            $stmt->execute();
            $row = $stmt->fetch();
            $stats['users']['active'] = $row->total;

            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `total` FROM `user_activity` WHERE `active` >= NOW() - INTERVAL 15 MINUTE'); 
            $stmt->execute();
            $row = $stmt->fetch();
            $stats['users']['online'] = $row->total;

            $stats['users']['new'] = array();
            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `total` FROM `users` WHERE `created` >= NOW() - INTERVAL 1 DAY'); 
            $stmt->execute();
            $row = $stmt->fetch();
            $stats['users']['new']['day'] = $row->total;

            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `total` FROM `users` WHERE `created` >= NOW() - INTERVAL 1 WEEK'); 
            $stmt->execute();
            $row = $stmt->fetch();
            $stats['users']['new']['week'] = $row->total;

            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) AS `total` FROM `users` WHERE `created` >= NOW() - INTERVAL 1 MONTH'); 
            $stmt->execute();
            $row = $stmt->fetch();
            $stats['users']['new']['month'] = $row->total;

            // Article stats
            $articles = new Articles();
            $stats['articles'] = $articles->getStats();

            // Level stats
            $stats['levels'] = array();
            $stats['levels']['count'] = \dtw\Playground::getLevelCount();
            $stats['levels']['completed'] = \dtw\Playground::getLevelsCompleted();

            // Forum stats
            $discussions = new Discussions();
            $stats['discussions'] = $discussions->getStats();

            // Queues
            $stats['queues'] = array();
            $stats['queues']['emailer'] = array(
                'queued' => utils\Queue::count('emails'),
                'processing' => utils\Queue::count('emails', true)
            );

            // Tickets
            $stats['tickets'] = \dtw\tickets\Tickets::getTicketCount();

            // Discussions
            $stats['discussions'] = (new \dtw\moderation\Discussions)->getQueues();

            return $stats;
        }

    }