<?php
    namespace dtw\playground\levels;

    abstract class BaseLevel {
        protected $data;

        public function __construct($level_id) {

            $this->messages = array();
            $this->messages['level.complete'] = 'Level completed';
            $this->messages['level.incorrect'] = 'Invalid login details';

            $this->redisKey = 'level:' . $level_id;

            $this->DtW = \dtw\DtW::getInstance();

            // Check if level exists in Redis
            $level = \dtw\DtW::$redis->get($this->redisKey);

            if ($level) {
                $this->data = json_decode($level);
            } else {
                $this->data = new \stdClass();

                // Load basic information
                $stmt = \dtw\DtW::$db->prepare('
                    SELECT `levels`.`title`, `levels`.`difficulty`, `levels`.`status`, `levels`.`slug`, `levels`.`subject`
                    FROM `levels`
                    WHERE `levels`.`level_id` = :ID AND `status` = "live"
                    LIMIT 1
                '); 
                $stmt->execute(array(':ID' => $level_id)); 
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();

                    $this->data->ID = $level_id;
                    $this->data->title = $row->title;
                    $this->data->status = $row->status;
                    $this->data->difficulty = $row->difficulty;
                    $this->data->slug = $row->slug;
                    $this->data->subject = $row->subject;

                    // Load the rest of the information
                    \dtw\DtW::$redis->set($this->redisKey, json_encode($this->data));
                } else {
                    throw new \Exception('Level not found');
                }
            }

            $this->data->template = 'basic';

            if (!$this->data->meta) {
                $this->getMeta();
            }

            if (isset($this->data->meta->answer) && (is_array($this->data->meta->answer) || is_object($this->data->meta->answer)) ) {
                $this->answer = $this->data->meta->answer;
            }
            unset($this->data->meta->answer);
        }

        // Load all non-required information
        public function getMeta() {
            $this->data->meta = new \stdClass();

            // Load basic information
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `key`, `value`
                FROM `level_meta`
                WHERE `level_meta`.`level_id` = :ID
            '); 
            $stmt->execute(array(':ID' => $this->data->ID));

            $rows = $stmt->fetchAll();

            foreach($rows AS $row) {
                if (json_decode($row->value)) {
                    $this->data->meta->{$row->key} = json_decode($row->value);
                } else {
                    $this->data->meta->{$row->key} = $row->value;
                }

                if ($row->key == 'description' || $row->key == 'extras') {
                    $purifier = new \dtw\utils\Purifier();
                    $value = \dtw\utils\Markdown::parse($row->value);
                    $value = $purifier->purify($value);
                    $this->data->meta->{$row->key . 'Safe'} = $value;
                }
            }

            // Get level stats
            $this->data->meta->stats = new \stdClass();

            // Get latest user to complete level
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `users`.`user_id`, `user_levels`.`finished`
                FROM `user_levels`
                INNER JOIN `users`
                ON `users`.`user_id` = `user_levels`.`user_id`
                WHERE `user_levels`.`level_id` = :ID AND `finished` IS NOT NULL
                ORDER BY `user_levels`.`finished` DESC
                LIMIT 2
            '); 
            $stmt->execute(array(':ID' => $this->data->ID));
            $row = $stmt->fetch();
            if ($row->user_id == 69) $row = $stmt->fetch();
            if ($row) {
                $this->data->meta->stats->last = array($row->user_id, $row->finished);
            }

            // Get first user to complete level
            $stmt = \dtw\DtW::$db->prepare('
                SELECT `users`.`user_id`, `user_levels`.`finished`
                FROM `user_levels`
                INNER JOIN `users`
                ON `users`.`user_id` = `user_levels`.`user_id`
                WHERE `user_levels`.`level_id` = :ID AND `finished` IS NOT NULL
                ORDER BY `user_levels`.`finished` ASC
                LIMIT 2
            '); 
            $stmt->execute(array(':ID' => $this->data->ID));
            $row = $stmt->fetch();
            if ($row->user_id == 69) $row = $stmt->fetch();
            if ($row) {
                $this->data->meta->stats->first = array($row->user_id, $row->finished);
            }

            // Load number of users started
            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(`user_id`) AS `started`
                FROM `user_levels`
                WHERE `level_id` = :ID
            '); 
            $stmt->execute(array(':ID' => $this->data->ID));
            $row = $stmt->fetch();
            $this->data->meta->stats->started = $row->started;

            // Load complete stats information
            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(`user_id`) AS `completed`
                FROM `user_levels`
                WHERE `level_id` = :ID AND `finished` IS NOT NULL
            '); 
            $stmt->execute(array(':ID' => $this->data->ID));
            $row = $stmt->fetch();
            $this->data->meta->stats->completed = $row->completed;

            $this->data->meta->stats->completedPercentage = 0;
            if ($this->data->meta->stats->completed > 0) {
                $this->data->meta->stats->completedPercentage = round($this->data->meta->stats->completed / $this->data->meta->stats->started * 100);
            }

            // Get last 5 days
            $this->data->meta->stats->graph = new \stdClass();
            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(*) `finished`
                FROM `user_levels`
                WHERE `finished` >= DATE_SUB(CURDATE(), INTERVAL 5 DAY) AND `level_id` = :ID
                GROUP BY DAY(`finished`)
            '); 
            $stmt->execute(array(':ID' => $this->data->ID));
            $row = $stmt->fetchAll(\PDO::FETCH_COLUMN);
            $this->data->meta->stats->graph->completed = json_encode($row);

            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(*) `started`
                FROM `user_levels`
                WHERE `started` >= DATE_SUB(CURDATE(), INTERVAL 5 DAY) AND `level_id` = :ID
                GROUP BY DAY(`started`)
            '); 
            $stmt->execute(array(':ID' => $this->data->ID));
            $row = $stmt->fetchAll(\PDO::FETCH_COLUMN);
            $this->data->meta->stats->graph->started = json_encode($row);

            \dtw\DtW::$redis->set($this->redisKey, json_encode($this->data));
        }
        
        public function getNextLevel() {
            // Get next level
            $this->DtW->load('Playground');
            $levels = \dtw\Playground::getLevelsOrdered();

            // Find current levels index
            $index = false;
            foreach($levels as $key => $tmpLevel) {
                if ($tmpLevel->ID == $this->data->ID) {
                    $index = $key;
                    break;
                }
            }

            // Store next levels slug
            if ($index >= 0 && array_key_exists($index + 1, $levels)) {
                if (!\dtw\DtW::getInstance()->user->levels->{$levels[$index + 1]->ID}->hasCompleted) {
                    $nextLevel = '/playground/' . $levels[$index + 1]->slug;
                }
            }

            if (!$nextLevel) {
                $topLevel = \dtw\Playground::getLevelsOrdered()[0];

                if (!\dtw\DtW::getInstance()->user->levels->{$topLevel->ID}->hasCompleted) {
                    $nextLevel = '/playground/' . $topLevel->slug;
                }
            }

            return $nextLevel;
        }

        public function getForm() {
            // Build default form
            if (isset($this->data->meta->form) && ($this->data->meta->form == 'userpass' || $this->data->meta->form == 'pass')) {
                $form = new \dtw\utils\Form($this->title, "Log in", array( 'hideTitle' => true, 'method' => 'post', 'action' => '', 'block' => false ));
                if ($this->data->meta->form == 'userpass') {
                    $form->addField('Username', "text");
                }
                $form->addField('Password', "password");
            }

            return $form;
        }

        public function getLink() {
            $completed = $this->hasCompleted();

            return sprintf(
                '<a href="/playground/%s">%s <i title="You %s completed this level" class="far fa-%s-circle c-%s"></i></a>',
                $this->slug,
                $this->title,
                $completed ? 'have' : 'have not',
                $completed ? 'check' : 'times',
                $completed ? 'primary' : 'error'
            );
        }

        public function updateLevel($data) {
            if (!$this->DtW->user->hasPrivilege('admin')) {
                throw new \Exception('You do not have privileges to complete this action');
            }

            $defaults = array(
                'status' => 'live',
                'difficulty' => 'bronze'
            );

            $requiredFields = array(
                'title'
            );
            
            // Check all required fields are present
            foreach ($requiredFields as $field) {
                if (!array_key_exists($field, $data) || !isset($data[$field]) || !$data[$field]) {
                    throw new \Exception('Missing fields');
                }
            }

            // Merge defaults and data
            $data = array_merge($defaults, $data);

            // Create level
            $stmt = \dtw\DtW::$db->prepare('
                UPDATE levels SET `title` = :title, `slug` = :slug, `difficulty` = :difficulty, `status` = :status, `subject` = :subject
                WHERE `level_id` = :level_id
            ');
            $stmt->execute(array(
                ':title' => $data['title'],
                ':slug' => $data['slug'],
                ':difficulty' => $data['difficulty'],
                ':status' => $data['status'],
                ':subject' => $data['subject'],
                ':level_id' => $this->data->ID
            ));

            // Create level meta
            $meta = array('description', 'class', 'form', 'answer', 'solution', 'extras', 'timer', 'online');

            // Tidy answer if present
            if (isset($data['answer']) && count($data['answer'])) {
                $answer = array();
                foreach($data['answer'] AS $tmpAnswer) {
                    if ($tmpAnswer['key'] && $tmpAnswer['value']) {
                        $answer[$tmpAnswer['key']] = $tmpAnswer['value'];
                    }
                }

                if (count($answer)) {
                    $data['answer'] = json_encode($answer);
                } else {
                    unset($data['answer']);
                }
            } else {
                unset($data['answer']);
            }

            foreach ($meta as $key) {
                if (isset($data[$key])) {
                    if ($data[$key]) {
                        $stmt = \dtw\DtW::$db->prepare('
                            INSERT INTO level_meta (`level_id`, `key`, `value`)
                            VALUES (:id, :key, :value)
                            ON DUPLICATE KEY UPDATE `value` = :value
                        ');
                        $stmt->execute(array(
                            ':id' => $this->data->ID,
                            ':key' => $key,
                            ':value' => $data[$key]
                        ));
                    } else {
                        $stmt = \dtw\DtW::$db->prepare('
                            DELETE FROM level_meta
                            WHERE `level_id` = :id AND `key` = :key
                        ');
                        $stmt->execute(array(
                            ':id' => $this->data->ID,
                            ':key' => $key
                        ));
                    }
                }
            }


            // Clear cache
            // TODO make this less aggressive
            \dtw\DtW::$redis->del('playground');
            \dtw\DtW::$redis->del('playground:stats');
            \dtw\DtW::$redis->del('level:class:' . $this->data->ID);
            \dtw\DtW::$redis->del($this->redisKey);
            //\dtw\DtW::$redis->flushall();

            return $data['slug'];
        }


        public function checkAnswer($answer) {
            if (!$this->DtW->user->isAuth()) throw new \Exception('User must be logged in to attempt this level');

            // print_r($this->answer);
            // echo ' | ';
            // print_r($answer);

            // Lower case answers
            $answer = array_map('strtolower', $answer);
            $this->answer = array_map('strtolower', (array) $this->answer);

            if (empty($this->answer) || empty($answer) || count(array_diff_assoc($this->answer, $answer))) {

                // Check if they are using their login username by mistake
                if (array_key_exists('username', $answer) && $answer['username'] == $this->DtW->user->username) {
                    return (object) array(
                        'type' => 'error',
                        'message' => 'You should not be entering your own login details here'
                    );
                }

                return (object) array(
                    'type' => 'error',
                    'message' => $this->messages['level.incorrect']
                );
            }

            $this->levelComplete();
            return (object) array(
                'type' => 'success',
                'message' => $this->messages['level.complete']
            );
        }

        public function onLevel() {
            $this->markAsViewed();
        }

        public function markAsViewed() {
            if (!$this->DtW->user->isAuth()) return;

            // Check if they already have a started
            if (isset($this->DtW->user->levels) && $this->DtW->user->levels->{$this->data->ID}) {
                return;
            }

            try {
                $stmt = \dtw\DtW::$db->prepare('
                    INSERT INTO `user_levels` (`user_id`, `level_id`)
                    VALUES (:user_id, :level_id)
                '); 
                $stmt->execute(array(':user_id' => $this->DtW->user->id, ':level_id' => $this->data->ID));

                // Clear session
                unset($_SESSION['userLevels']);
            } catch (\Exception $e) {
                // Users level data already setup
            }
        }

        /**
         * Mark level as completed
         */
        protected function levelComplete() {
            if (!$this->DtW->user->isAuth()) return;

            if ($this->hasCompleted()) {
                return;
            }

            try {
                $stmt = \dtw\DtW::$db->prepare('
                    UPDATE `user_levels` SET `finished` = NOW() WHERE `user_id` = :user_id AND `level_id` = :level_id AND `finished` IS NULL
                '); 
                $stmt->execute(array(':user_id' => $this->DtW->user->id, ':level_id' => $this->data->ID));

                // Clear level meta
                $this->purgeCache();

                // Update users level session
                $this->DtW->user->levels->regenerate();

                // Update user
                $this->DtW->user->profile->getLevels(true);
                \dtw\Medals::checkUsersMedals('levels');
                $this->DtW->user->profile->calculateReputation(true);

                // Add to feed
                \dtw\utils\Feed::addItemToFeed(array(
                    'type' => 'playground.complete',
                    'user' => $this->DtW->user->id,
                    'level' => $this->data->title,
                    'permalink' => "/playground/" . $this->data->slug
                ));

                // Update wechall
                file_get_contents("https://wechall.net/remoteupdate.php?sitename=ht&username=" . $this->DtW->user->username);
            } catch (\Exception $e) {
                // Error storing data
                // var_dump($e);
                die();
            }
        }

        public function hasCompleted() {
            return (isset($this->DtW->user->levels) && $this->DtW->user->levels->{$this->data->ID} && $this->DtW->user->levels->{$this->data->ID}->hasCompleted);
        }

        public function saveNotes($notes) {
            \dtw\user\Meta::set('playground:notes:' . $this->data->ID, $notes);
        }

        public function getNotes($html = false) {
            $notes = \dtw\user\Meta::get('playground:notes:' . $this->data->ID);

            if ($html) {
                $notes = \dtw\utils\Markdown::parse($notes, true);
            }

            return $notes;
        }

        public function getNotesForm() {
            $form = new \dtw\utils\Form('Notes', 'Save', array(
                'action' => "/playground/" . $this->data->slug . "/notes",
                'description' => 'These notes are private and will not be displayed to any other users. They will also be accessible from any linked discussion thread.'
            ));
            $form->addField('Notes', 'markdown', array(
                'hideLabel' => true,
                'name' => 'notes',
                'value' => $this->getNotes()
            ));

            return $form;
        }

        public function purgeCache() {
            unset($this->data->meta);
            \dtw\DtW::$redis->set($this->redisKey, json_encode($this->data));
        }

        protected function generatePassword() {
            $key = $this->redisKey . ':password';

            if (isset($_SESSION[$key])) {
                return $_SESSION[$key];
            }

            $password = \dtw\utils\Utils::generateToken(10);
            $_SESSION[$key] = $password;

            return $password;
        }

        protected function generateUsername($stored = true) {
            if ($stored) {
                $key = $this->redisKey . ':username';
                if (isset($_SESSION[$key]) && $_SESSION[$key]) {
                    return $_SESSION[$key];
                }
            }

            $usernames = file(realpath(dirname($_SERVER['DOCUMENT_ROOT'], 2) . '/config/usernames.txt'));
            $username = trim($usernames[rand(0, count($usernames) - 1)]);

            if ($stored) {
                $_SESSION[$key] = $username;
            }

            return $username;
        }

        public function __get($name) {
            if ($name == 'data') {
                return $this->data;
            }

            if (property_exists($this->data, $name)) {
                return $this->data->$name;
            }

            return null;
        }

        public function __isset($name) {
            return property_exists($this->data, $name);
        }
    }