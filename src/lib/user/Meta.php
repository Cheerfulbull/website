<?php
    namespace dtw\user;

    class Meta {
        private $_meta;

        public static function get($key, $userID = null) {
            if ($userID == null) {
                if (!\dtw\DtW::getInstance()->user->isAuth()) {
                    return;
                }

                if (isset($_SESSION['meta']) && isset($_SESSION['meta'][$key])) {
                    return $_SESSION['meta'][$key];
                }

                $currentUser = true;
                $userID = \dtw\DtW::getInstance()->user->id;
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT `value`
                FROM `user_meta`
                WHERE `user_id` = :id AND `key` = :key
            '); 
            $stmt->execute(array(
                ':id' => $userID,
                ':key' => $key
            ));

            $value = $stmt->fetch(\PDO::FETCH_COLUMN);

            if ($currentUser) {
                if (!isset($_SESSION['meta'])) {
                    $_SESSION['meta'] = array();
                }
                $_SESSION['meta'][$key] = $value;
            }

            return $value;
        }

        public static function set($key, $value, $update = true, $userID = null) {
            if ($userID == null) {
                if (!\dtw\DtW::getInstance()->user->isAuth()) {
                    return;
                }

                $currentUser = true;
                $userID = \dtw\DtW::getInstance()->user->id;
            }

            $sql = 'INSERT INTO `user_meta` (`user_id`, `key`, `value`) VALUES (:id, :key, :value)';

            if ($update) {
                $sql .= ' ON DUPLICATE KEY UPDATE `value` = :value';
            }

            try {
                $stmt = \dtw\DtW::$db->prepare($sql); 
                $stmt->execute(array(
                    ':id' => $userID,
                    ':key' => $key,
                    ':value' => $value
                ));

                if (!$update && $stmt->rowCount()) {
                    // Check todo medal
                    \dtw\Medals::checkUsersMedals('todo', $userID);
                }

                if ($currentUser) {
                    if (!isset($_SESSION['meta'])) {
                        $_SESSION['meta'] = array();
                    }
                    $_SESSION['meta'][$key] = $value;
                }

                return true;
            } catch (\Exception $e) {
                return false;
            }
        }


    }