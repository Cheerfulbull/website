<?php
    namespace dtw;

    class Medal extends \dtw\utils\DataObject {
        protected $type = 'medal';
        protected $table = 'medals';

        protected function getByslug($label, $colour = 'bronze') {
            // Lookup label in Redis
            $medalID = \dtw\DtW::$redis->get($this->type . ':label:' . $label);

            // If the link wasn't found, check DB
            if (!$medalID) {
                $stmt = \dtw\DtW::$db->prepare('SELECT medal_id FROM medals WHERE `label` = :label AND `colour` = :colour'); 
                $stmt->execute(array(
                    ':label' => $label,
                    ':colour' => $colour
                ));
                if ($stmt->rowCount()) {
                    $row = $stmt->fetch();
                    $medalID = $row->medal_id;
                } else {
                    throw new \Exception('Medal not found');
                }
            }

            try {
                $this->getByID($medalID);
            } catch (\Exception $e) {
                throw $e;
            }
        }

        protected function getByID($id) {
            // Check if medal exists in Redis
            $medal = \dtw\DtW::$redis->get($this->type . ':' . $id);

            if ($medal) {
                $this->data = json_decode($medal);
                \dtw\DtW::$log->info('medal.view', array('title' => $this->data->label, 'source' => 'redis'));
            } else {
                // Rebuild article from DB
                $stmt = \dtw\DtW::$db->prepare('SELECT `medal_id` AS `id`, `label`, `colour`, `description`, `type`, `count`, `order` FROM medals WHERE `medal_id` = :medalID'); 
                $stmt->execute(array(':medalID' => $id)); 
                if (!$stmt->rowCount()) {
                    throw new \Exception('Medal not found');
                }
                $this->data = $stmt->fetch();

                $this->updateCache();
                \dtw\DtW::$log->info('medal.view', array('title' => $this->data->label, 'source' => 'DB'));
            }

            $this->id = $this->data->id;
        }

        public function hasCompleted($userID = null) {
            if (!$userID) {
                $DtW = \dtw\DtW::getInstance();
                if (!$DtW->user->isAuth()) {
                    return false;
                }
                $profile = $DtW->user->profile;
            } else {
                $profile = \dtw\Users::getUser($userID);
            }

            return in_array($this->id, $profile->getMedals());
        }

        public function getMyProgress() {
            $DtW = \dtw\DtW::getInstance();

            if (!$DtW->user->isAuth()) {
                return false;
            }

            // if ($this->data->progress) {
            //     return $this->data->progress;
            // }

            $progress = new \stdClass();
            $progress->completed = $this->hasCompleted();
            $progress->stepsTotal = $this->getStepsTotal();

            if (!$this->data->type) {
                return $progress;
            }

            if ($progress->completed) {
                $progress->percentage = 100;
                $progress->stepsCompleted = $progress->stepsTotal;
            } else {
                $progress->stepsCompleted = $this->getStepsCompleted();
                $progress->stepsCompleted = $progress->stepsCompleted < 0 ? 0 : $progress->stepsCompleted;
                if ($progress->stepsTotal) {
                    $progress->percentage = ($progress->stepsCompleted / $progress->stepsTotal) * 100;
                }

                if ($progress->percentage >= 100 || (!$progress->stepsTotal && $progress->stepsCompleted)) {
                    $this->awardMedal($DtW->user->id);

                    $progress->completed = true;                    
                }
            }

            if ($DtW->user->nextMedal->id == $this->id) {
                $DtW->user->setNextMedal($progress);
            }

            return $progress;
        }

        public function getUserProgress($userID) {
            $DtW = \dtw\DtW::getInstance();

            $progress = new \stdClass();
            $progress->stepsTotal = $this->getStepsTotal();

            $progress->stepsCompleted = $this->getStepsCompleted($userID);
            if ($progress->stepsCompleted === null) {
                throw new \Exception('Cant calculate for non logged in member');
            }

            if ($progress->stepsTotal) {
                $progress->percentage = ($progress->stepsCompleted / $progress->stepsTotal) * 100;
            }

            if ($progress->percentage >= 100 || (!$progress->stepsTotal && $progress->stepsCompleted)) {
                $this->awardMedal($userID);
            } else if ($progress->stepsTotal) {
                $this->removeMedal($userID);
            }

            if ($userID == $DtW->user->id && $DtW->user->nextMedal->id == $this->id) {
                $DtW->user->setNextMedal($progress);
            }

            return $progress;
        }

        public function awardMedal($userID = null) {
            $DtW = \dtw\DtW::getInstance();

            if (!$userID) {
                $userID = $DtW->user->id;
            }

            // Award user medal
            try {
                $stmt = \dtw\DtW::$db->prepare("
                    INSERT INTO `user_medals` (`user_id`, `medal_id`)
                    VALUES ( :user_id, :medal_id);
                ");
                $stmt->execute(array(
                    ':medal_id' => $this->id,
                    ':user_id' => $userID
                ));

                if ($stmt->rowCount() == 1) {
                    // Notify user
                    $notificationData = (object) array(
                        'medal' => $this->id
                    );
                    \dtw\Notifications::send($userID, 'medal.awarded', $notificationData);

                    // Update users session
                    if ($userID == $DtW->user->id) {
                        unset($_SESSION['userMedals']);
                    }

                    if ($userID == $DtW->user->id && $DtW->user->nextMedal->id == $this->id) {
                        $DtW->user->setNextMedal(null);
                    }

                    // Clear user profile
                    \dtw\Users::purgeUser($userID);

                    $this->purgeStats();
                }
            } catch (\Exception $e) {
                // Already awarded
            }
        }

        public function removeMedal($userID = null) {
            $DtW = \dtw\DtW::getInstance();

            if (!$userID) {
                $userID = $DtW->user->id;
            }

            // Only remove certain medals
            if (!$this->type || in_array($this->type, array('logins', 'member', 'todo', 'days'))) {
                return;
            }

            // Remove user medal
            try {
                $stmt = \dtw\DtW::$db->prepare("
                    DELETE FROM `user_medals` 
                    WHERE `user_id` = :user_id AND `medal_id` = :medal_id;
                ");
                $stmt->execute(array(
                    ':medal_id' => $this->id,
                    ':user_id' => $userID
                ));

                if ($stmt->rowCount() == 1) {
                    // Update users session
                    if ($userID == $DtW->user->id) {
                        unset($_SESSION['userMedals']);
                    }

                    // Remove notifications
                    $notificationData = (object) array(
                        'medal' => $this->id
                    );
                    \dtw\Notifications::delete($userID, 'medal.awarded', $notificationData);

                    // Clear user profile
                    \dtw\Users::purgeUser($userID);

                    $this->purgeStats();
                }
            } catch (\Exception $e) {
                //
            }
        }

        public function getStats() {
            $key = $this->type . ':stats:' . $this->id;
            $stats = \dtw\DtW::$redis->get($key);

            if ($stats) {
                return json_decode($stats);
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT count(*) AS `awarded`,min(awarded) AS `first`, max(awarded) AS `latest`
                FROM user_medals
                WHERE medal_id = :medal_id
            ');
            $stmt->execute(array(
                ':medal_id' => $this->id
            ));

            $stats = $stmt->fetch();

            \dtw\DtW::$redis->set($key, json_encode($stats));

            return $stats;
        }

        private function purgeStats() {
            $key = $this->type . ':stats:' . $this->id;
            \dtw\DtW::$redis->del($key);
        }

        public function getStepsTotal() {
            if ($this->data->count && $this->data->count > 0) {
                return $this->data->count;
            }

            $DtW = \dtw\DtW::getInstance();
            if ($this->data->type == 'levels') {
                return $this->getStepsTotalLevels();
            }
        }

        private function getStepsCompleted($userID = null) {
            if ($this->data->type == 'levels') {
                return $this->getStepsCompletedLevels($userID);
            } else if ($this->data->type == 'posts') {
                return $this->getStepsCompletedForumPosts($userID);
            } else if ($this->data->type == 'member') {
                return $this->getStepsCompletedMember($userID);
            } else if ($this->data->type == 'todo') {
                return $this->getStepsCompletedTodo($userID);
            } else if ($this->data->type == 'flags') {
                return $this->getStepsCompletedFlags($userID);
            } else if ($this->data->type == 'articles') {
                return $this->getStepsCompletedArticles($userID);
            } else if ($this->data->type == 'logins') {
                return $this->getStepsCompletedLogins($userID);
            } else if ($this->data->type == 'donate') {
                return $this->getStepsCompletedDonate($userID);
            } else if ($this->data->type == 'followers') {
                return $this->getStepsCompletedFollowers($userID);
            } else if ($this->data->type == 'following') {
                return $this->getStepsCompletedFollowing($userID);
            } else if ($this->data->type == 'days') {
                return $this->getStepsCompletedDays($userID);
            } else if ($this->data->type == 'karma') {
                return $this->getStepsCompletedKarma($userID);
            }

            return 0;
        }

        private function getStepsTotalLevels() {
            $difficulty = $this->data->count;
            return array_reduce(\dtw\Playground::getLevels(), function($carry, $item) use ($difficulty) {
                $level = \dtw\Playground::getByID($item->ID);
                if ($level->difficulty == $difficulty) {
                    return $carry + 1;
                }
                return $carry;
            }) | 0;
        }

        private function getStepsCompletedLevels($userID) {
            $DtW = \dtw\DtW::getInstance();
            $difficulty = $this->data->count;

            if ($userID !== null) {
                $profile = \dtw\Users::getUser($userID);
                $completed = $profile->levels;
            } else {
                $completed = $DtW->user->profile->levels;
            }

            return array_reduce($completed, function($carry, $item) use ($DtW, $difficulty) {
                try {
                    $level = \dtw\Playground::getByID($item->level_id);
                    if ($level->difficulty == $difficulty) {
                        return $carry + 1;
                    }
                } catch (\Exception $e) {
                    // Level removed    
                }
                return $carry;
            }) | 0;
        }

        private function getStepsCompletedForumPosts($userID) {
            $DtW = \dtw\DtW::getInstance();
            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) FROM `forum_thread_posts` WHERE `author` = :userID AND deleted = 0'); 
            $stmt->execute(array(
                ':userID' => ($userID === null ? $DtW->user->id : $userID)
            ));
            $posts = $stmt->fetchColumn();

            return $posts;
        }

        private function getStepsCompletedMember($userID = null) {
            $DtW = \dtw\DtW::getInstance();
            $profile = \dtw\Users::getUser($userID === null ? $DtW->user->id : $userID);
            $created = $profile->created;

            $days = floor((time() - strtotime($created)) / 86400);
            return $days;
        }

        private function getStepsCompletedTodo($userID = null) {
            $completed = 0;

            $DtW = \dtw\DtW::getInstance();
            $profile = \dtw\Users::getUser($userID === null ? $DtW->user->id : $userID);
            
            // Update profile
            $completed += $profile->bio || $profile->status ? 1 : 0;

            // Completed a level
            $completed += count($profile->levels) ? 1 : 0;

            // Posted in discussion
            $completed += $this->getStepsCompletedForumPosts($userID) ? 1 : 0;

            // Read code of conduct
            $completed += \dtw\user\Meta::get('view:help:conduct', $userID) ? 1 : 0;

            // Read an article
            $completed += \dtw\user\Meta::get('view:article', $userID) ? 1 : 0;

            return $completed;
        }

        private function getStepsCompletedFlags($userID = null) {
            $DtW = \dtw\DtW::getInstance();
            if (!$userID) {
                $userID = $DtW->user->id;
            }

            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) FROM forum_flags WHERE (`resolved` = 1 or `resolved` = 3) AND `user_id` = :userID'); 
            $stmt->execute(array(
                ':userID' => $userID
            ));
            $posts = $stmt->fetchColumn();

            return $posts;
        }

        private function getStepsCompletedArticles($userID = null) {
            $DtW = \dtw\DtW::getInstance();
            if (!$userID) {
                $userID = $DtW->user->id;
            }

            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) FROM articles WHERE `author` = :userID AND `status` = "published"'); 
            $stmt->execute(array(
                ':userID' => $userID
            ));
            $posts = $stmt->fetchColumn();

            return $posts;
        }

        private function getStepsCompletedLogins($userID = null) {
            $DtW = \dtw\DtW::getInstance();
            if (!$userID) {
                $userID = $DtW->user->id;
            }

            $stmt = \dtw\DtW::$db->prepare('SELECT `consecutive_max` FROM `user_activity` WHERE `user_id` = :userID'); 
            $stmt->execute(array(
                ':userID' => $userID
            ));
            $consecutive = $stmt->fetchColumn();

            return $consecutive;
        }

        private function getStepsCompletedDonate($userID = null) {
            $DtW = \dtw\DtW::getInstance();
            if (!$userID) {
                $userID = $DtW->user->id;
            }

            $stmt = \dtw\DtW::$db->prepare('select sum(`amount`) from user_donations where user_id = :userID'); 
            $stmt->execute(array(
                ':userID' => $userID
            ));
            $amount = $stmt->fetchColumn();

            return $amount ? $amount : 0;
        }

        private function getStepsCompletedDays($userID = null) {
            $DtW = \dtw\DtW::getInstance();
            if (!$userID) {
                $userID = $DtW->user->id;
            }

            $stmt = \dtw\DtW::$db->prepare('SELECT `days` FROM `user_activity` WHERE `user_id` = :userID'); 
            $stmt->execute(array(
                ':userID' => $userID
            ));
            $days = $stmt->fetchColumn();

            return $days;
        }

        private function getStepsCompletedFollowers($userID = null) {
            $DtW = \dtw\DtW::getInstance();
            if (!$userID) {
                $userID = $DtW->user->id;
            }

            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) FROM `user_following` WHERE `following` = :userID'); 
            $stmt->execute(array(
                ':userID' => $userID
            ));
            $followers = $stmt->fetchColumn();

            return $followers;
        }

        private function getStepsCompletedFollowing($userID = null) {
            $DtW = \dtw\DtW::getInstance();
            if (!$userID) {
                $userID = $DtW->user->id;
            }

            $stmt = \dtw\DtW::$db->prepare('SELECT count(*) FROM `user_following` WHERE `user_id` = :userID'); 
            $stmt->execute(array(
                ':userID' => $userID
            ));
            $followers = $stmt->fetchColumn();

            return $followers;
        }

        private function getStepsCompletedKarma($userID = null) {
            $DtW = \dtw\DtW::getInstance();
            if (!$userID) {
                $userID = $DtW->user->id;
            }

            $stmt = \dtw\DtW::$db->prepare('
                SELECT SUM(case when `vote` = "up" then 1 else -1 end) as `karma`
                FROM forum_karma
                INNER JOIN `forum_thread_posts`
                ON `forum_thread_posts`.post_id = `forum_karma`.post_id
                WHERE `forum_thread_posts`.`author` = :userID GROUP BY `forum_thread_posts`.`author`'); 
            $stmt->execute(array(
                ':userID' => $userID
            ));
            $karma = $stmt->fetchColumn();

            return $karma;
        }

    }