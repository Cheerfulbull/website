<?php
    namespace dtw;

    class DtW {
        public static $log = null;
        public static $config = null;
        public static $version = null;
        public static $db = null;
        public static $redis = null;
        public static $api = null;
        public static $cron = null;

        public static function getInstance($basic = false) {
            static $instance = null;
            if ($instance === null) {
                $instance = new DtW();
                $instance->init($basic);
                // \dtw\DtW::$log->info(__CLASS__, array('msg' => "Creating new DtW Instance"));
            }
            return $instance;
        }

        private function __construct() {
            define('DIR', __DIR__ . '/../'); 
        }

        private function init($basic = false) {
            // Config
            try {
                static::$config = \Noodlehaus\Config::load(realpath(dirname($_SERVER['DOCUMENT_ROOT'], 2) . '/config/config.json'));
            } catch (\Noodlehaus\Exception\FileNotFoundException $e) {
                die('Config file not found');
            }
            static::$version = trim(@file_get_contents(realpath(dirname($_SERVER['DOCUMENT_ROOT'], 2) . '/config/version.txt')));

            if (static::$config->get('debug')) {
                ini_set('display_errors', 1);
            }

            // Load API
            static::$api = new utils\Api;

            // Load Cron
            $cron = new utils\Cron;
            static::$cron = $cron;

            // Logger
            static::$log = new utils\Log;

            // Load DB
            static::$db = new utils\Database;

            // Load Redis
            $redis = new utils\Redis;
            static::$redis = $redis->client;


            if (!$basic) {
                $this->user = new user\User;

                $this->tmpl = new utils\Tmpl;

                $this->tmpl->twig->addGlobal('dark', (\dtw\user\Meta::get('style.dark') == 'true') ? true : false);

                if ($this->user->isAuth()) {
                    $this->tmpl->twig->addGlobal('user', $this->user);
                    $this->tmpl->twig->addGlobal('CSRFtoken', \dtw\utils\CSRF::generate());
                }
            }
        }

        // Load extra modules
        public function load($lib) {
            $lib = preg_replace('/[^A-Za-z\-]/', '', $lib);
            $key = lcfirst($lib);

            if (!property_exists($this, $key)) {
                $path = "\dtw\\" . $lib;
                $this->$key = new $path;
            }
        }

        public function getSidebar() {
            $sidebar = \dtw\DtW::$redis->get('sidebar');

            if ($sidebar) {
                $sidebar = unserialize($sidebar);
            } else {
                // Get sidebar from config
                $sidebar = \Noodlehaus\Config::load(__DIR__ . '/../../config/sidebar.json')->all();

                // Load articles
                try {
                    array_push($sidebar['articles']['items'], array(
                        'title' => 'All topics',
                        'url' => '/articles'
                    ));

                    $topics = \dtw\Articles::getTopics();
                    foreach($topics AS $topic) {
                        $c = array();
                        $c['title'] = $topic->title;
                        $c['url'] = $topic->permalink;

                        if ($topic->sidebar) {
                            array_push($sidebar['articles']['items'], $c);
                        }
                    }
                } catch (\Exception $e) {
                    // No topics
                }

                self::$redis->set('sidebar', serialize($sidebar));
            }

            // Check privilages
            // TODO refactor
            foreach($sidebar AS $sectionKey => $section) {
                // Check items
                if (array_key_exists('items', $section)) {
                    foreach($section['items'] AS $itemKey => $item) {
                        if (array_key_exists('privileges', $item)) {
                            if ($item['privileges'] == "user" && $this->user->isAuth()) {
                                continue;
                            }

                            if ($item['privileges'] == "guest" && !$this->user->isAuth()) {
                                continue;
                            }

                            if ($this->user->isAuth() && $this->user->hasPrivilege($item['privileges'])) {
                                continue;
                            }

                            unset($sidebar[$sectionKey]['items'][$itemKey]);
                        }

                        // Check children
                        if (array_key_exists('children', $item)) {
                            foreach($item['children'] AS $childKey => $child) {
                                if (array_key_exists('privileges', $child)) {
                                    if ($child['privileges'] == "user" && $this->user->isAuth()) {
                                        continue;
                                    }

                                    if ($child['privileges'] == "guest" && !$this->user->isAuth()) {
                                        continue;
                                    }

                                    if ($this->user->isAuth() && $this->user->hasPrivilege($child['privileges'])) {
                                        continue;
                                    }

                                    unset($sidebar[$sectionKey]['items'][$itemKey]['children'][$childKey]);
                                }
                            }

                            if (is_array($sidebar[$sectionKey]['items'][$itemKey]['children']) && count($sidebar[$sectionKey]['items'][$itemKey]['children']) === 0 && !array_key_exists('url', $item)) {
                                unset($sidebar[$sectionKey]['items'][$itemKey]);
                            }
                        }
                    }
                }
            }

            return $sidebar;
        }
    }
?>